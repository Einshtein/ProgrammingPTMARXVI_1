package ua.org.oa.makhnonosove.Practice1;

import org.junit.Test;

import static org.junit.Assert.*;


public class ArrayProdTest {


    @Test
    public void testSum() throws Exception {
        int [] num  = {1,2,3,4};
        int expected = 24;
        int actual = ArrayProd.sum(num);
        assertEquals("Должно быть "+ expected,expected,actual,0.01);
    }
}