package ua.org.oa.makhnonosove.Practice1;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


public class ArraySumTest {

    @Test
    public void testSum() throws Exception {
        int [] num  = {1,2,3,4};
        int expected = 10;
        int actual = ArraySum.sum(num);
        assertEquals("Должно быть "+ expected,expected,actual,0.01);
    }

    ArraySum arraySum;
    @Before
    public void setUp() throws Exception {
        int [] numConstructor = {1,2,3,4,5};
        arraySum = new ArraySum(numConstructor);
    }

    @Test
    public void testSum1() throws Exception {
        int expected = 15;
        int actual = arraySum.sum();
        assertEquals("Должно быть "+ expected,expected,actual,0.01);
    }

    @Test(expected = java.lang.NullPointerException.class)
    public void testSum2() throws Exception {
        ArraySum.sum(null);

    }
}