package ua.org.oa.makhnonosove.Practice1;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ArrayProdTest.class, ArraySumTest.class})
public class AllTests {}
