package ua.org.oa.makhnonosove.HomeWork2;

import org.hamcrest.StringDescription;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Евгений on 09.04.2016.
 */
public class StringUtilsTest {

    StringUtils stringUtils;
    List<String> list;
    @Before
    public void setUp() throws Exception {
        stringUtils = new StringUtils();
        list = new ArrayList<>();
        list.add("+73453121314");
        list.add("+13562133567");
    }

    @Test
    public void testStringReverseSB() throws Exception {
        assertEquals("ушаМ тибюл ясаВ", stringUtils.stringReverseSB("Вася любит Машу"));
    }

    @Test
    public void testIsPalindromeString() throws Exception {
        assertEquals(true, stringUtils.isPalindromeString("А роза упала на лапу Азора"));
    }

    @Test
    public void testStringLengthTest() throws Exception {
        assertEquals("KolyaPetyaoo", stringUtils.stringLengthTest("KolyaPetya"));
    }

    @Test
    public void testReplaceFirstWordWithLast() throws Exception {
        assertEquals("маму любит свою Вася", stringUtils.replaceFirstWordWithLast("Вася любит свою маму"));
    }

    @Test
    public void testReplaceFirstWordWithLastInEachSentense() throws Exception {
       assertArrayEquals(new String[]{"маму любит свою Вася.", "Васю мама А"}, stringUtils.replaceFirstWordWithLastInEachSentense("Вася любит свою маму.А мама Васю"));
    }

    @Test
    public void testFindABC() throws Exception {
        assertTrue(stringUtils.findABC("aaababbbcbccb"));
    }

    @Test
    public void testDateCheker() throws Exception {
        assertTrue(stringUtils.dateCheker("12.12.2014"));
    }

    @Test
    public void testEmailChecker() throws Exception {
        assertTrue(stringUtils.emailChecker("vasya@pupkin.ru"));
    }

    @Test
    public void testPhoneNumberSearcher() throws Exception {
        assertEquals(list,
                stringUtils.phoneNumberSearcher("Kolya number: +7(345)312-13-14, Vasya number: +1(356)213-35-67"));
        }


}
