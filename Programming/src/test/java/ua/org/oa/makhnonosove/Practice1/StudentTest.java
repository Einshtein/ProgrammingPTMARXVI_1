package ua.org.oa.makhnonosove.Practice1;

import org.junit.Before;
import org.junit.Test;
import ua.org.oa.makhnonosove.Practice1.Exceptions.ExamNameException;
import ua.org.oa.makhnonosove.Practice1.Exceptions.ExamNotFoundException;
import ua.org.oa.makhnonosove.Practice1.Exceptions.MarkException;
import ua.org.oa.makhnonosove.Practice1.Exceptions.SemestrException;

import java.time.LocalDateTime;

import static org.junit.Assert.*;
/**
 * Created by Евгений on 04.04.2016.
 */
public class StudentTest {

        Student student;
    @Before
    public void setUp() throws Exception, ExamNameException, MarkException, SemestrException {
        student = new Student("Vasya", "Ivanov", new Group(4, "IT"),
                new Exam[]{new Exam("Physics", LocalDateTime.of(2016, 12, 18, 14, 30), 4, 1),
                        new Exam("Math", LocalDateTime.of(2016, 12, 10, 14, 30), 4, 1),
                        new Exam("Programming", LocalDateTime.of(2016, 12, 06, 14, 30), 5, 2)});
    }

    @Test
    public void testGetHighestMark(){
        assertEquals(4,student.getHighestMark("Physics"));
    }

    @Test
    public void testSetMark() throws Exception, MarkException {
        assertEquals(5,student.setNewMark("Physics", LocalDateTime.parse("2016-12-18T14:30"), 5));
    }

    @Test
    public void testDeleteMark() throws Exception, MarkException {
        assertEquals(true,student.deleteMark("Physics",LocalDateTime.parse("2016-12-18T14:30")));
    }

    @Test(expected = ExamNotFoundException.class)
    public void testExamException() throws ExamNotFoundException {
        student.examException("Literature");
    }

    @Test
    public void testExamQuantity() throws Exception {
        assertEquals(2,student.examQuantity(4));
    }
    @Test
    public void testAverageMark() throws Exception {
        assertEquals(4.0,student.averageMark(1),0.01);
    }
}

