package ua.org.oa.makhnonosove.HomeWork1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Евгений on 08.04.2016.
 */
public class DateTest {
    Date date;
    Date.Year year;
    Date.Month month;
    Date.Day day;
    @Before
    public void setUp() throws Exception {
        date = new Date(1, 9, 2016);
        year = date.new Year();
       month = date.new Month();

    }

    @Test
    public void testGetDayOfWeek() throws Exception {
        assertEquals(day.THURSDAY,Date.Day.valueOf(date.getDayOfWeek()));
    }

    @Test
    public void testDaysBetween() throws Exception {
        assertEquals(4,date.daysBetween(new Date(5, 9, 2016)));
    }

    @Test
    public void testIsLeap() throws Exception {
        assertTrue(year.isLeap());
    }
    @Test
    public void testGetDaysOfMonth() throws Exception {
        assertEquals(30,month.getDaysOfMonth(date.getMonth()));
    }

}