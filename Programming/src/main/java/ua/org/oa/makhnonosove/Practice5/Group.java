package ua.org.oa.makhnonosove.Practice5;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Евгений on 17.05.2016.
 */
public class Group implements Serializable{
    private static final long serialVersionUID = -2809228405533313732L;
    private List<Student> studentList = new ArrayList<>();

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public void addStudents(Student student)
    {
        studentList.add(student);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Group: ");
        for (Student student : studentList) {
            sb.append(student).append("\n");
        }
        return sb.toString();
    }
}
