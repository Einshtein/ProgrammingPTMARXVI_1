package ua.org.oa.makhnonosove.Practice1;


public class ArrayProd {

    /**
     * @param num
     * @return
     */
    //Метод суммирования массива
    public static int sum(int[] num) {
        int sum = 1;
        for (int i : num) {
            sum *= i;
        }
        return sum;
    }
}
