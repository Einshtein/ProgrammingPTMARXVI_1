package ua.org.oa.makhnonosove.Practice3;

import java.util.Date;
import java.util.List;

/**
 * Created by Евгений on 17.04.2016.
 */
public class User {
    private long id;
    private String name;
    private String login;
    private String password;
    private Date birthday;
    private List<Book> books;

 public User(Date birthday, List<Book> books, long id, String login, String name, String password) {
  setBirthday(birthday);
  setBooks(books);
  setId(id);
  setLogin(login);
  setName(name);
  setPassword(password);
 }
 public  User(){}

 public Date getBirthday() {
  return birthday;
 }

 public void setBirthday(Date birthday) {
  this.birthday = birthday;
 }

 public List<Book> getBooks() {
  return books;
 }

 public void setBooks(List<Book> books) {
  this.books = books;
 }

 public void addBooks (Book book) { books.add(book);}

 public long getId() {
  return id;
 }

 public void setId(long id) {
  this.id = id;
 }

 public String getLogin() {
  return login;
 }

 public void setLogin(String login) {
  this.login = login;
 }

 public String getName() {
  return name;
 }

 public void setName(String name) {
  this.name = name;
 }

 public String getPassword() {
  return password;
 }

 public void setPassword(String password) {
  this.password = password;
 }



}
