package ua.org.oa.makhnonosove.Practice1.Exceptions;

/**
 * Created by Евгений on 04.04.2016.
 */
public class ExamNotFoundException extends Throwable {
    public ExamNotFoundException(String message) {
        super(message);
    }
}
