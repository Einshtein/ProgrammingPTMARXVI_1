package ua.org.oa.makhnonosove.HomeWork5;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Евгений on 21.05.2016.
 */
public class Translator {
    public static void main(String[] args) throws FileNotFoundException {
        //меню
        int dictionaryNumber = 0;
        String path = null;
        m:
        for (;;) {
            System.out.println("Выберите направление перевода");
            dictionaries();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(new UnclouseSystemIn(System.in)))) {
                try {
                    dictionaryNumber = Integer.parseInt(reader.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if(readFromFiletoVocabulary(dictionaryNumber) != null) {
                        System.out.println("Введите путь к файлу");
                        path = reader.readLine();
                        translate(path, readFromFiletoVocabulary(dictionaryNumber));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println("Продолжить? y/n");
                String choice = reader.readLine();
                switch (choice) {
                    case "y":
                        break;
                    case "n":
                        break m;
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

    }
    //массив словарей
        static List<String> dictionaries = new ArrayList<>();
    //метод для определения словарей по именам файлов
        public static List<String> dictionaries() {
        File file = new File("C:\\Users\\Евгений\\IdeaProjects\\Programming\\src\\main\\resourses\\HomeWork5\\Dictionaries");
        if (file.exists()) {
            int i = 1;
            File[] fileNames = file.listFiles();
            for (File fileName : fileNames) {
                System.out.println(i + " - " + fileName.getName());
                i++;
                dictionaries.add(fileName.toString());
            }
        }
        return dictionaries;
    }
    //метод для считывания словарей из файлов в Map
    public static Map<String,String> readFromFiletoVocabulary (int dictionaryNumber) {
        Map<String,String> vocabulary = new HashMap<>();
            try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(dictionaries.get(dictionaryNumber - 1))))) {
                try {
                    String s;
                    while ((s = br.readLine()) != null) {
                        String[] buffer = s.split("=");
                        if (!vocabulary.containsKey(buffer[0])) {
                            vocabulary.put(buffer[0], buffer[1]);
                        }
                    }
                } finally {
                    br.close();
                }
//                for (Map.Entry<String, String> entry : vocabulary.entrySet()) {
//                    System.out.println(entry.getKey());
//                }
            } catch (IndexOutOfBoundsException i){
                System.out.println("Такого индекса нет! Выберите из предложенных!");
                return null;
            }
            catch (IOException e) {
                throw new RuntimeException(e);

            }
        return vocabulary;
    }
    //метод для перевода
    public static void translate (String path, Map<String,String> vocabulary) {
                try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path)))) {
                    String s;
                    System.out.println("Перевод: ");
                    while ((s = br.readLine()) != null) {
                        Pattern p = Pattern.compile("(?<word>\\w+)(?<notWord>\\W*)",Pattern.UNICODE_CHARACTER_CLASS);
                        Matcher m = p.matcher(s);
                        while (m.find()) {
                            if (vocabulary.containsKey(m.group("word"))) {
                                System.out.print(vocabulary.get(m.group("word")));
                            }
                            System.out.print(m.group("notWord"));
                        }
                        System.out.println();
                        System.out.println();
                    }
            } catch (IOException e) {
                    System.out.println("Файл не найден");
            }


    }

}
