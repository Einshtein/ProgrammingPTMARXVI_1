package ua.org.oa.makhnonosove.Practice5;



import java.io.*;
import java.util.*;

/**
 * Created by Евгений on 14.05.2016.
 */
public class App {
    public static void main(String[] args) throws IOException {
        writeToFile(randomIntGenerator(0,100, 20), "RandomInt");
        writeToFile(intSort("RandomInt"), "RandomInt");
        studentList("Students", 90);
        writeToFile(wordReplace("Sentences"), "Sentences");
        System.out.println(copyFileBuffered("Source", "DestinationBuffer"));
        System.out.println(copyFileNotBuffered("Source", "DestinationNotBuffer"));
    }

    public static void writeToFile(String s, String path) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter (new FileOutputStream(path, true)));){
            bw.write(s);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String readFromFile(String path) throws FileNotFoundException {
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
            try {
                String s;
                while ((s = br.readLine()) != null) {
                    sb.append(s);
                    sb.append("\n");
                }
            } finally {
                br.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return sb.toString();
    }

    public static String randomIntGenerator(int min, int max, int qty) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < qty; i++) {
            Integer rnd = min + (int) (Math.random() * ((max - min) + 1));
            if (i != qty-1)
            sb = sb.append(rnd).append(" ");
        }
        sb.append("\n");
        return sb.toString();
    }

    public static String intSort(String path) throws FileNotFoundException {
        StringBuilder sb= new StringBuilder();
        String[] splitString = readFromFile(path).split("[ \n]+");
        List<Integer> sortedList = new ArrayList<>();
        for (int i = 0; i < splitString.length; i++) {
           sortedList.add(Integer.valueOf(splitString[i]));
        }
        Collections.sort(sortedList);
        for (int i = 0; i < sortedList.size(); i++) {
            if (sortedList.get(i) != sortedList.get(sortedList.size()-1))
            sb.append(sortedList.get(i)).append(" ");
        }
        return sb.toString();
    }

    public static void studentList (String path, int rating){
        class Student{
            private List<Integer> marks = new ArrayList<>();
            public Student addMarks (int mark)
            {
                marks.add(mark);
                return this;
            }
            public int averageMark (){
                int sum = 0;
                int average = 0;
                for (Integer integer : marks) {
                    sum += integer;
                }
                average = sum/marks.size();
                return average;
            }

            @Override
            public String toString() {
                final StringBuilder sb = new StringBuilder("Student ");
                sb.append("marks =").append(marks);
                return sb.toString();
            }
        }

        Map<String, Student> studentMap = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path)));){
            String s;
            while ((s = br.readLine()) != null){
                String[] buffer = s.split("=");
                if (!studentMap.containsKey(buffer[0])){
                    studentMap.put(buffer[0], new Student().addMarks(Integer.parseInt(buffer[1])));
                } else {
                    studentMap.get(buffer[0]).addMarks(Integer.parseInt(buffer[1]));
                }

            }
            for (Map.Entry<String, Student> stringStudentEntry : studentMap.entrySet()) {
                System.out.println(stringStudentEntry.getKey() + " - " + stringStudentEntry.getValue());
            }
            System.out.println("Студенты со средним балом выше " + rating + ":");
            for (Map.Entry<String, Student> stringStudentEntry : studentMap.entrySet()) {
                if (stringStudentEntry.getValue().averageMark() > rating) {
                    System.out.println(stringStudentEntry.getKey());
                }
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static String wordReplace (String path){
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path)));){
            String s;

            while ((s = br.readLine()) != null) {
                String[] buffer = s.split("[\n]+");
                    for (int i = 0; i < buffer.length; i++) {
                        sb.append(replaceFirstWordWithLastInEachSentense(buffer[i]).trim()).append("\n");
                    }
            }
            System.out.println(sb);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    private static String replaceFirstWordWithLastInEachSentense(String str) {
        StringBuilder sb = new StringBuilder();
        String[] str2 = str.split("[.]+");
        String[] str3 = new String[str2.length];
        for (int i = 0; i < str2.length; i++) {
                str2[i] = str2[i].trim();
            int firstSpace = str2[i].indexOf(" ");
            int lastSpace = str2[i].lastIndexOf(" ");
            String firstWord = str2[i].substring(0, firstSpace);
            String lastWord = str2[i].substring(lastSpace+1, str2[i].length());
            String rest = str2[i].substring(firstSpace, lastSpace + 1);
            str3[i] = lastWord.concat(rest).concat(firstWord);
        }
        for (int i = 0; i < str3.length; i++) {
                str3[i] = str3[i].concat(".").concat(" ");
            sb.append(str3[i]);
        }
        return sb.toString();
    }

    public static Long copyFileBuffered (String sourse, String destination){
        Long start = System.nanoTime();
        for (int i = 0; i <10 ;) {
        try {
            writeToFile(readFromFile(sourse), destination);
            i++;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        }
        Long finish = System.nanoTime();
        return finish - start;
    }

    public static Long copyFileNotBuffered (String sourse, String destination) throws IOException {
        Long start = System.nanoTime();
        for (int i = 0; i <10 ; i++) {
            StringBuilder sb = new StringBuilder();
            try (FileReader br = new FileReader(sourse);) {
                int c;
                while ((c = br.read()) != -1) {
                    sb.append((char) c);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            try (FileWriter bw = new FileWriter(destination, true);) {
                bw.write(sb.toString());

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Long finish = System.nanoTime();
        return finish - start;
    }




}
