package ua.org.oa.makhnonosove.Practice6;

/**
 * Created by Евгений on 25.05.2016.
 */
public class ThreadNames {
    public static void main(String[] args) {
        runThread1();
        runThread2();
    }
    //создаем поток путем расширения класса Thread
    static class Thread1 extends Thread {
        public Thread1(String name){
            setName(name);
        }
        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                try {
                    System.out.println(Thread.currentThread().getName());
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Первый способ завершен");
        }
    }

    public static void runThread1() {
        System.out.println("Первый способ");
        Thread thread1 = new Thread1("Первый поток");
        thread1.start();

    }

    //создаем поток путем реализации интерфейса Runnable
    static class Thread2 implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                try {
                    System.out.println(Thread.currentThread().getName());
                    Thread.currentThread().sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Второй способ завершен");
        }
    }

    public static void runThread2() {
        System.out.println("Второй способ");
        Thread thread2 = new Thread(new Thread2());
        thread2.start();
    }
}
