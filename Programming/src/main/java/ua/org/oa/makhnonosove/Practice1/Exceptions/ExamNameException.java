package ua.org.oa.makhnonosove.Practice1.Exceptions;

/**
 * Created by Евгений on 05.04.2016.
 */
public class ExamNameException extends Throwable {
    public ExamNameException(String message) {
        super(message);
    }
}
