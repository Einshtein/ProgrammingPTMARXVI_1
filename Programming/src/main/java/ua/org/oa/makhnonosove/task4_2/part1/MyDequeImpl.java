package ua.org.oa.makhnonosove.task4_2.part1;

import java.util.Arrays;
import java.util.NoSuchElementException;

/**
 * Created by Евгений on 07.05.2016.
 */
public class MyDequeImpl<E> implements MyDeque<E> {
    private int size = 0;
    private Node<E> first;
    private Node<E> last;
    @Override
    public void addFirst(E e) {
        Node<E> f = first;
        Node newNode = new Node(e, f, null);
        if (first == null) {
            first = newNode;
            last = newNode;
        }
        else {
            newNode.next = first;
            first = newNode;
        }
        size++;
    }


    @Override
    public void addLast(E e) {
        Node<E> l = last;
        Node newNode = new Node(e, null, l);
        if (last == null) {
            first = newNode;
            last = newNode;
        }
        else{
            last.next = newNode;
            last = newNode;
        }
        size++;
    }


    @Override
    public E removeFirst() {
        Node<E> f = first;
        if (f == null)
            throw new NoSuchElementException();
        E elem = f.element;
        Node<E> next = f.next;
        f.element = null;
        f.next = null;
        first = next;
        if (next == null)
            last = null;
        else
            next.prev = null;
        size--;
        return elem;
    }

    @Override
    public E removeLast() {
        Node<E> l = last;
        if (l == null)
            throw new NoSuchElementException();
        E elem = l.element;
        Node<E> prev = l.prev;
        l.element = null;
        l.prev = null;
        last = prev;
        if (prev == null)
            first = null;
        else
            prev.next = null;
        size--;
        return elem;
    }

    @Override
    public E getFirst() {
        if (first == null)
            throw new NoSuchElementException();
        return first.element;
    }

    @Override
    public E getLast() {
        if (last == null)
            throw new NoSuchElementException();
        return last.element;
    }

    @Override
    public boolean contains(Object o) {
            for (Node<E> x = first; x != null; x = x.next) {
                if (o.equals(x.element))
                    return true;
            }
        return false;
    }

    @Override
    public void clear() {
            for (Node<E> x = first; x != null; x= x.next) {
                x.element = null;
                x.next = null;
                x.prev = null;
            }
            first = last = null;
            size = 0;
        }


    @Override
    public Object[] toArray() {
        Object[] result = new Object[size];
        int i = 0;
        for (Node<E> x = first; x != null; x = x.next) {
            result[i] = x.element;
            i++;
        }
        return result;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean containsAll(MyDeque<? extends E> deque) {
        Object[] a = deque.toArray();
        int count = 0;
        if (a.length <= toArray().length) {
            for (int i = 0; i < toArray().length; i++) {
                for (int j = 0; j < a.length; j++) {
                    if (a[i].equals(this.toArray()[j])) {
                        count++;
                    }
                }
            }
            return count == toArray().length;
        }
        else return false;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("{");
        for (Node<E> x = first; x != null; x = x.next) {
            if (x != last)
                result.append(x.element.toString()).append(", ");
            else result.append(x.element.toString());
        }
        result.append('}');
        return result.toString();
    }

    private static class Node<E> {

// хранимый элемент

        private E element;

// ссылка на следующий элемент списка

        private Node<E> next;

// ссылка на предыдущий элемент списка

        private Node<E> prev;

        public E getElement() {
            return element;
        }

        public void setElement(E element) {
            this.element = element;
        }

        public Node<E> getNext() {
            return next;
        }

        public void setNext(Node<E> next) {
            this.next = next;
        }

        public Node<E> getPrev() {
            return prev;
        }

        public void setPrev(Node<E> prev) {
            this.prev = prev;
        }

        public Node(E element, Node<E> next, Node<E> prev) {
            this.element = element;
            this.next = next;
            this.prev = prev;
        }

        public Node() {
        }
    }
}