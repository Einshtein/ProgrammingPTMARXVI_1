package ua.org.oa.makhnonosove.Practice6;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

/**
 * Created by Евгений on 26.05.2016.
 */
public class CopyFile extends Thread {
    public static String COPY_FROM_PATH = "CopyFrom";
    public static String COPY_TO_PATH = "CopyTo";
    //метод для сканирования заданной папки и копирования ее структуры в указанную
    public static void searchFiles(String COPY_FROM_PATH, String COPY_TO_PATH) {
        File[] sourseList = new File(COPY_FROM_PATH).listFiles();
        for (File file : sourseList) {
            if (file.isFile()) {
                for (int i = 0; i < sourseList.length; i++) {
                    String s = COPY_TO_PATH.concat("\\").concat(sourseList[i].getName());
                    File newFile = new File(s);
                    try {
                        Files.copy(sourseList[i].toPath(), newFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    } catch (FileSystemException fse) {
                        //
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (file.isDirectory()) {
                new Thread(() -> searchFiles(file.getAbsolutePath(), COPY_TO_PATH + "/" + file.getName())).start();
            }
        }
    }

    @Override
    public void run() {
               searchFiles(COPY_FROM_PATH, COPY_TO_PATH);
    }

    public static void main(String[] args) {
        Thread copyFile = new CopyFile();
        copyFile.start();
    }
}
