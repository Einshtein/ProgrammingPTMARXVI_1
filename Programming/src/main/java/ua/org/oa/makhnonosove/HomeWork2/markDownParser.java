package ua.org.oa.makhnonosove.HomeWork2;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Евгений on 09.04.2016.
 */
public class MarkDownParser {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static void main(String[] args) throws IOException {
        MarkDownParser reader = new MarkDownParser();
        reader.readFromFile();
        String result = reader.makeHeader(reader.getText());
        result = reader.wrapString(result);
        result = reader.makeItalic(result);
        result = reader.makeStrong(result);
        result = reader.makeLink(result);
        reader.writeToFile(result);
    }

    public String readFromFile() throws FileNotFoundException {
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("C:\\Users\\Евгений\\IdeaProjects\\Programming\\MarkDownParserSourse.txt"), "windows-1251"));
            try {
                String s;
                while ((s = br.readLine()) != null) {
                    sb.append(s);
                    sb.append("\n");
                }
            } finally {
                br.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        setText(sb.toString());
        return sb.toString();
    }

    public void writeToFile(String s) throws IOException {
        FileWriter writer = new FileWriter("C:\\Users\\Евгений\\IdeaProjects\\Programming\\Text1.html", false);
        writer.write(s);
        writer.close();
    }

    public String makeHeader(String s) {
        Pattern header1 = Pattern.compile("#(\\w+\\s?)+");
        Pattern header2 = Pattern.compile("#{2}(\\w+\\s?)+");
        Pattern header3 = Pattern.compile("#{3}(\\w+\\s*)+");
        Pattern header4 = Pattern.compile("#{4}(\\w+\\s*)+");
        Pattern header5 = Pattern.compile("#{5}(\\w+\\s*)+");
        Pattern header6 = Pattern.compile("#{6}(\\w+\\s*)+");
        String[] array = s.split("[\n]+");
        StringBuilder sb = new StringBuilder();
        sb.append("<html>\n");
        sb.append("<body>\n");

        for (String ss : array) {
            Matcher m1 = header1.matcher(ss);
            Matcher m2 = header2.matcher(ss);
            Matcher m3 = header3.matcher(ss);
            Matcher m4 = header4.matcher(ss);
            Matcher m5 = header5.matcher(ss);
            Matcher m6 = header6.matcher(ss);
            if (m1.matches()) {
                ss = ss.replace("#", "");
                sb.append("<h1>").append(ss).append("</h1>").append("\n");
            } else if (m2.matches()) {
                ss = ss.replace("##", "");
                sb.append("<h2>").append(ss).append("</h2>").append("\n");
            } else if (m3.matches()) {
                ss = ss.replace("###", "");
                sb.append("<h3>").append(ss).append("</h3>").append("\n");
            } else if (m4.matches()) {
                ss = ss.replace("####", "");
                sb.append("<h4>").append(ss).append("</h4>").append("\n");
            } else if (m5.matches()) {
                ss = ss.replace("#####", "");
                sb.append("<h5>").append(ss).append("</h5>").append("\n");
            } else if (m6.matches()) {
                ss = ss.replace("######", "");
                sb.append("<h6>").append(ss).append("</h6>").append("\n");
            } else {
                sb.append(ss);
                sb.append("\n");
            }
        }
        sb.append("</body>\n");
        sb.append("</html>\n");
        return sb.toString();
    }

    public String wrapString(String s) {
        String[] array = s.split("\n");
        StringBuilder sb = new StringBuilder();
        Pattern p = Pattern.compile("<.+");
        for (String str : array) {
            Matcher m = p.matcher(str);
            if (!m.matches()) {
                sb.append("<p>");
                sb.append(str);
                sb.append("</p>\n");
            } else {
                sb.append(str);
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    public String makeItalic(String s) {
        Pattern p = Pattern.compile("-\\w+-");
        Matcher m = p.matcher(s);
        while (m.find()) {
            s = s.replace(m.group(), "<em>" + m.group().replace("-", "") + "</em>");
        }
        return s;
    }

    public String makeStrong(String s) {
        Pattern p = Pattern.compile("\\*\\w+\\*");
        Matcher m = p.matcher(s);
        while (m.find()) {
            s = s.replace(m.group(), "<strong>" + m.group().replace("*", "") + "</strong>");
        }
        return s;
    }

    public String makeLink(String s) {
        String temp;
        Pattern p = Pattern.compile("(\\[.+\\])(\\(.+\\))");
        Matcher m = p.matcher(s);
        while (m.find()) {
            temp = m.group(1) + m.group(2);
            s = s.replace(temp, "<a href=\"" + m.group(2).replace("(", "").replace(")", "") + "\">" + m.group(1).replace("[", "").replace("]", "") + "</a>");
        }
        return s;
    }

}
