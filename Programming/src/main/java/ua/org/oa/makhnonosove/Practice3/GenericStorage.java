package ua.org.oa.makhnonosove.Practice3;

/**
 * Created by Евгений on 18.04.2016.
 */
public class GenericStorage<K,V> {
    private class Node<K,V>{
        private K key;
        private V value;
          public Node(K key, V value) {
            this.key = key;
            this.value = value;
          }
            public K getKey() {return key;}
            public void setKey(K key) {
            this.key = key;
            }
            public V getValue() {
            return value;
            }
            public void setValue(V value) {
            this.value = value;
           }
            @Override
            public String toString() {
            return "Node{" +
                    "key=" + key +
                    ", value=" + value +
                    '}';
            }
        }
    private Node <K,V> [] arr; //массив
    private int index = 0; //индекс текущего элемента массива
    //конструктор без параметров задает размер 10
    public GenericStorage() {
        this.setArr((Node <K,V> []) new Object[10]);
    }
    //конструктор с размером
    public GenericStorage(int size) {
        this.setArr((Node <K,V> []) new Object[size]);
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Node <K,V> [] getArr() {
        return arr;
    }

    public void setArr(Node <K,V> [] arr) {
        this.arr = arr;
    }

    public boolean add(Node <K,V> obj) {
//        setArr(Arrays.copyOf(getArr(), getArr().length + 1));//увеличиваем длинну массива на 1
        if (getIndex() <= getArr().length) {
            getArr()[getIndex()] = obj;
            setIndex(getIndex() + 1);//увеличиваем индекс на 1
        }
        return true;
    }

}
