package ua.org.oa.makhnonosove.Practice2;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Евгений on 11.04.2016.
 */
public class ComputerApp {
    public static void main(String[] args) throws FileNotFoundException {
        ComputerApp parser = new ComputerApp();
        parser.readFromFile();
        parser.parse();
        System.out.println(ComputerApp.toStringNew(computers));
    }

    private String INPUTFILEPATH = "source.html";
    private static  List<Computer> computers = new ArrayList();

    public static List<Computer> getComputers() {
        return computers;
    }

    public static void addComputers(Computer computer) {
        computers.add(computer);
    }


    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public  String readFromFile () throws FileNotFoundException {
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(INPUTFILEPATH),"windows-1251"));
            try {
                String s;
                while ((s = br.readLine()) != null) {
                    sb.append(s);
                    sb.append("\n");
                }
            } finally {
                br.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        setText(sb.toString());
        return sb.toString();
    }

    public List<Computer> parse(){

        Pattern pricePattern = Pattern.compile("<span class=\"price cost\">(?<price>\\d+)[\\w+\\W+]+?<\\/span>");
        Matcher priceMatcher = pricePattern.matcher(getText());

        Pattern modelPattern = Pattern.compile("<h6 class=\"name\"><a[\\w+\\W+]+?>(?<model>[\\w+\\W+]+?)<\\/a><\\/h6>");
        Matcher modelMatcher = modelPattern.matcher(getText());

            Pattern descriptionPattern = Pattern.compile("<p class=\"description\">\\s?(?<screen>\\d+,\\d+)\"\\s+\\/\\s+" +
                    "(?<resolution>\\d+.+?\\d+?)\\s+\\/[\\w+\\W+]+?(?<processor>\\w+\\d+-?\\d+\\w+?,?\\s*\\d+?,?\\d+[ГГц]?)[\\w+\\W+]+?" +
                    "(?<memory>\\d+)\\s+Гб\\s+[\\w+\\W+]+?-\\s(?<HDD>\\d+)\\s+[ТГ]б[\\w+\\W+]+?<\\/p>");
        Matcher descriptionMatcher = descriptionPattern.matcher(getText());

        while (priceMatcher.find() & modelMatcher.find() & descriptionMatcher.find()) {
            Computer computer = new Computer();
            computer.setPrice(Double.parseDouble(priceMatcher.group("price")));
            computer.setModel(modelMatcher.group("model"));
            computer.setScreenDiagonal(Double.parseDouble(descriptionMatcher.group("screen").replaceAll(",",".")));
            computer.setResolution(descriptionMatcher.group("resolution"));
            computer.setCore(descriptionMatcher.group("processor"));
            computer.setMemoryCapacity(Integer.parseInt(descriptionMatcher.group("memory")));
            computer.setHdd(Integer.parseInt(descriptionMatcher.group("HDD")));
            addComputers(computer);
        }
        return computers;
    }

    public static String toStringNew(List<Computer> computers){
        StringBuilder sb = new StringBuilder();
        for (Computer computer : computers) {
                sb.append(computer);
                sb.append('\n');

        }
        return sb.toString();
    }
}
