package ua.org.oa.makhnonosove.Practice1.Exceptions;

/**
 * Created by Евгений on 06.04.2016.
 */
public class SemestrException extends Throwable {
    public SemestrException(String message) {
        super(message);
    }
}
