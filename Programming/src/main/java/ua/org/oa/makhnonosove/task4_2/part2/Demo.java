package ua.org.oa.makhnonosove.task4_2.part2;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Евгений on 07.05.2016.
 */
public class Demo {

    public static void main(String[] args) {

        MyDeque<Number> deque = new MyDequeImpl<Number>();

        deque.addFirst(42343);
        deque.addFirst(453);
        deque.addLast(8.88);
        deque.addLast(345);
        deque.addLast(234234);
        System.out.println(deque);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~");
        for (Number element : deque) { System.out.println(element); }

        Iterator<Number> it = deque.iterator();
        while (it.hasNext()) {
                it.next();
                it.remove();
            if (deque.size()<=2)
                break;
        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println(deque);


    }

}