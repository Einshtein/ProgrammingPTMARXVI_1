package ua.org.oa.makhnonosove.HomeWork7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalTime;

/**
 * Created by Евгений on 25.05.2016.
 */
public class ThreadsApp {
    public static void main(String[] args) {
        interruptThread1();
        interruptThread2();
    }
    //создаем поток путем расширения класса Thread
      static class Thread1 extends Thread {
            @Override
            public void run() {
                while (!Thread.currentThread().isInterrupted()) {
                    try {
                        System.out.println(LocalTime.now().getHour() + ":" + LocalTime.now().getMinute() + ":" + LocalTime.now().getSecond());
                        sleep(1000);
                    } catch (InterruptedException e) {
                        System.out.println("Поток прерван");
                        Thread.currentThread().interrupt();
                    }
                }
            }

        }

    //метод для прерывания потока вводом с клавиатуры
    public static void interruptThread1() {
        Thread thread1 = new Thread1();
        thread1.start();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            reader.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        thread1.interrupt();
    }
    //создаем поток путем реализации интерфейса Runnable
    static class Thread2 implements Runnable {
        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    System.out.println(LocalTime.now().getHour() + ":" + LocalTime.now().getMinute() + ":" + LocalTime.now().getSecond());
                    Thread.currentThread().sleep(1000);
                } catch (InterruptedException e) {
                    System.out.println("Поток прерван");
                    Thread.currentThread().interrupt();
                }
            }
        }

    }
    //метод для прерывания потока вводом с клавиатуры
    public static void interruptThread2() {
        Thread thread2 = new Thread(new Thread2());
        thread2.start();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            reader.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        thread2.interrupt();
    }
}


