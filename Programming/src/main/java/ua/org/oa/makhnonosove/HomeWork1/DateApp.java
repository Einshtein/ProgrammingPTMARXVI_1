package ua.org.oa.makhnonosove.HomeWork1;

/**
 * Created by Евгений on 06.04.2016.
 */
public class DateApp {
    public static void main(String[] args) {
        //инициализация всех объектов
        Date date = new Date(1,9,2016);
        System.out.println("Выбранная дата: " + date.getDay() + " " + date.getMonth() + " " + date.getYear());
        Date.Year year = date.new Year();
        Date.Month month = date.new Month();
        //вызов методов
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Проверка высокосный ли год: ");
        year.isLeap();
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Количество дней в месяце: ");
        System.out.println(month.getDaysOfMonth(date.getMonth()));
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("День недели: ");
        Date.Day day = Date.Day.valueOf(date.getDayOfWeek());
        System.out.println(day);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Количество дней между заданными датами:");
        Date date1 = new Date(5,9,2016);
        System.out.println("Дата №1: " + date.getDay() + " " + date.getMonth() + " " + date.getYear());
        System.out.println("Дата №2: " + date1.getDay() + " " + date1.getMonth() + " " + date1.getYear());
        System.out.println("Количество дней: ");
        System.out.println(date.daysBetween(date1));
    }
}
