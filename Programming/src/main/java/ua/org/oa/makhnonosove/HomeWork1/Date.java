package ua.org.oa.makhnonosove.HomeWork1;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Евгений on 06.04.2016.
 */
public class Date {
    //поля
    private int day;
    private int month;
    private int year;
    //геттеры и сеттеры
    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
    //конструктор с параметрами
    public Date(int day, int month, int year) {
        setDay(day);
        setMonth(month);
        setYear(year);
    }
    //пустой конструктор
    public Date(){}
    //метод для нахождени порядкового номера дня недели
    public int getDayOfWeek(){
        GregorianCalendar gregorianCalendar = new GregorianCalendar(getYear(), getMonth()-1, getDay());
        int dayOfWeekNumber = (gregorianCalendar.get(Calendar.DAY_OF_WEEK)-2);
        return dayOfWeekNumber;
    }
    public long daysBetween(Date date) {
        long daysBetween = 0;
        Calendar first = GregorianCalendar.getInstance();
        first.set(this.getYear(), this.getMonth(), this.getDay());
        Calendar second = GregorianCalendar.getInstance();
        second.set(date.getYear(), date.getMonth(), date.getDay());
        daysBetween = (second.getTimeInMillis() - first.getTimeInMillis())/1000/3600/24;
        return daysBetween;
    }

    class Year {
        private boolean leapYear;

        public void setLeapYear(boolean leapYear) {
            this.leapYear = leapYear;
        }

        public boolean getLeapYear() {
            return leapYear;
        }
        //метод для определения высокосности года
        public boolean isLeap() {
            int d = year % 400; //проверка на кратностьк года 400
            int b = year % 100; //проверка на кратностьк года 100
            int s = year % 4; //проверка на кратностьк года 4
            if (s == 0 & b > 0 || d == 0) //если год кратен 4, не кратен 100 или кратен 400 - то он високосный
            {
                leapYear = true;
                System.out.println("Год высокосный");
                return true;
            } else {
                leapYear = false;
                System.out.println("Год не высокосный");
                return false;
            }
        }

    }

    class Month {
        Year testYear = new Year();
        //метод для опеределения количества дней в заданном месяце
        public int getDaysOfMonth(int month) {
            switch (month) {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    return 31;
                case 2:
                    if (testYear.isLeap())
                        return 29;
                    if (!testYear.isLeap())
                        return 28;
                case 4:
                case 6:
                case 9:
                case 11:
                    return 30;
                default:
                    return 30;
            }

        }
    }

    public enum Day {
        MONDAY(0), TUESDAY(1), WEDNESDAY(2), THURSDAY(3), FRIDAY(4), SATURDAY(5), SUNDAY(6);
        public int index;

        private Day(int index) {
            this.index = index;
        }

        public static Day valueOf(int index) {
            switch (index) {
                case 0:
                    return MONDAY;
                case 1:
                    return TUESDAY;
                case 2:
                    return WEDNESDAY;
                case 3:
                    return THURSDAY;
                case 4:
                    return FRIDAY;
                case 5:
                    return SATURDAY;
                case 6:
                    return SUNDAY;
            }
            return null;
        }

    }
}
