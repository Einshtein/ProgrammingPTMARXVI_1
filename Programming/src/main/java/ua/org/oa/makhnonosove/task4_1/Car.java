package ua.org.oa.makhnonosove.task4_1;

/**
 * Created by Евгений on 01.05.2016.
 */
public class Car {
    private String name;
    private int tank;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTank() {
        return tank;
    }

    public void setTank(int tank) {
        this.tank = tank;
    }

    public Car(String name, int tank) {
        setName(name);
        setTank(tank);
    }

}
