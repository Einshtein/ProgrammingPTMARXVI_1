package ua.org.oa.makhnonosove.Practice1;

import ua.org.oa.makhnonosove.Practice1.Exceptions.ExamNotFoundException;
import ua.org.oa.makhnonosove.Practice1.Exceptions.MarkException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;

/**
 * Created by Евгений on 04.04.2016.
 */
public class Student {
    private String name;
    private String surname;
    private Group group;
    private Exam[] exam;

    public Exam[] getExam() {
        return exam;
    }



    public void setExam(Exam[] exam) {
        this.exam = exam;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }


    public Student(String name, String surname, Group group, Exam[] exam) {
        this.exam = exam;
        this.group = group;
        this.name = name;
        this.surname = surname;
    }

    public int getHighestMark (String examName) {
        int highestMark = 0;
        for (Exam exam1 : exam) {
            if (exam1.getExamName().equals(examName)) {
                if (highestMark < exam1.getMark()) {
                    highestMark = exam1.getMark();
                }
            }
        }
        return highestMark;
    }

    public int setNewMark(String examName, LocalDateTime date, int mark) throws MarkException {
        for (Exam exam1 : exam) {
            if (exam1.getExamName().equals(examName) & exam1.getDate().equals(date)) {
                exam1.setMark(mark);
                return mark;
            }
        }
        return -1;
    }

    public boolean deleteMark(String examName, LocalDateTime date) throws MarkException {
        for (Exam exam1 : exam) {
            if (exam1.getExamName().equals(examName) & exam1.getDate().equals(date)) {
                exam1.setMark(0);
                return true;
            }
        }
        return false;
    }

    public boolean examException(String examName) throws ExamNotFoundException {
        for (Exam exam1 : exam) {
            if (!Arrays.asList(exam).contains(examName)) {
                throw new ExamNotFoundException("Такой экзамен данный студент не здавал!");
            }
        }
        return true;
    }

    public int examQuantity(int mark) {
        int examQuantity = 0;
        for (Exam exam1 : exam) {
            if (exam1.getMark() == mark) {
                examQuantity++;
            }
        }
        return examQuantity;
    }

    public double averageMark(int semestr) {
        double averageMark = 0;
        double totalMark = 0;
        double semestrMark = 0;
        for (Exam exam1 : exam) {
            if (exam1.getSemestr() == semestr) {
                totalMark += exam1.getMark();
                semestrMark++;
            }
            }
            averageMark = new BigDecimal(totalMark / semestrMark).setScale(2, RoundingMode.UP).doubleValue();
        return averageMark;
    }
}
