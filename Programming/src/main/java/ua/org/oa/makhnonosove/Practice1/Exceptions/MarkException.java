package ua.org.oa.makhnonosove.Practice1.Exceptions;

/**
 * Created by Евгений on 05.04.2016.
 */
public class MarkException extends Throwable {
    public MarkException(String message) {
        super(message);
    }
}
