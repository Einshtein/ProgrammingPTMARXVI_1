package ua.org.oa.makhnonosove.HomeWork3;

/**
 * Created by Евгений on 16.04.2016.
 */
public class App {
    public static void main(String[] args) {
        //создаем объект
        GenericStorage<String> listString = new GenericStorage();
        //добавляем элементы в массив
        listString.add("First");
        listString.add("Second");
        listString.add("Second");
        listString.add("Fourth");
        listString.add("Fifth");
        //выполняем методы
        System.out.println("~~~~~~~~~~~~~~~~~");
        System.out.println(listString.getAll());
        System.out.println("~~~~~~~~~~~~~~~~~");
        listString.get(2);
        System.out.println("~~~~~~~~~~~~~~~~~");
        listString.update(0,"newElement");
        System.out.println(listString.getAll());
        System.out.println("~~~~~~~~~~~~~~~~~");
        listString.delete(3);
        System.out.println(listString.getAll());
        System.out.println("~~~~~~~~~~~~~~~~~");
        listString.delete("Second");
        System.out.println(listString.getAll());
        System.out.println("~~~~~~~~~~~~~~~~~");
        listString.usedArrayLength();



        GenericStorage<Integer> numberString = new GenericStorage(5);
        //добавляем элементы в массив
        numberString.add(1);
        numberString.add(2);
        numberString.add(2);
        numberString.add(4);
        numberString.add(5);
        //выполняем методы
        System.out.println("~~~~~~~~~~~~~~~~~");
        System.out.println("~~~~~~~~~~~~~~~~~");
        System.out.println(numberString.getAll());
        System.out.println("~~~~~~~~~~~~~~~~~");
        numberString.get(2);
        System.out.println("~~~~~~~~~~~~~~~~~");
        numberString.update(0,45);
        System.out.println(numberString.getAll());
        System.out.println("~~~~~~~~~~~~~~~~~");
        numberString.delete(3);
        System.out.println(numberString.getAll());
        System.out.println("~~~~~~~~~~~~~~~~~");
        numberString.delete(Integer.valueOf("2"));
        System.out.println(numberString.getAll());
        System.out.println("~~~~~~~~~~~~~~~~~");
        numberString.usedArrayLength();
    }
}

