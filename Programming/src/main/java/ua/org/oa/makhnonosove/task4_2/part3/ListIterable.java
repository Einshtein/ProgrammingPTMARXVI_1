package ua.org.oa.makhnonosove.task4_2.part3;

/**
 * Created by Евгений on 14.05.2016.
 */
public interface ListIterable<E> {

    ListIterator<E> listIterator();

}
