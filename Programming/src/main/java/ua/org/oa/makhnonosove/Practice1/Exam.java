package ua.org.oa.makhnonosove.Practice1;

import ua.org.oa.makhnonosove.Practice1.Exceptions.ExamNameException;
import ua.org.oa.makhnonosove.Practice1.Exceptions.MarkException;
import ua.org.oa.makhnonosove.Practice1.Exceptions.SemestrException;

import java.time.LocalDateTime;

/**
 * Created by Евгений on 04.04.2016.
 */
public class Exam {
    private String examName;
    private int mark;
    private LocalDateTime date;
    private int semestr;
    private static final int MIN_EXAMNAME_LENGTH = 2;

    public int getSemestr() {
        return semestr;
    }

    public void setSemestr(int semestr) throws SemestrException {
        if (semestr > 0)
        this.semestr = semestr;
        else
            throw new SemestrException("Cеместр должен быть положительным!");
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) throws ExamNameException {
        if (examName.length() >= MIN_EXAMNAME_LENGTH)
        this.examName = examName;
        else
            throw new ExamNameException("Название экзамена не менее " + MIN_EXAMNAME_LENGTH);
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) throws MarkException {
        if (mark>=0)
        this.mark = mark;
        else
            throw new MarkException("Оценка должна быть положительной!");
    }

    public Exam(String examName, LocalDateTime date, int mark, int semestr) throws ExamNameException, MarkException, SemestrException {
       setDate(date);
       setExamName(examName);
       setMark(mark);
       setSemestr(semestr);
    }
}
