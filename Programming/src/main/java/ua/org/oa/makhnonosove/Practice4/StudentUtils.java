package ua.org.oa.makhnonosove.Practice4;

import java.util.*;

/**
 * Created by Евгений on 24.04.2016.
 */
public class StudentUtils {
    public static void main(String[] args) {
        Student vasya = new Student(4, "Vasya", "Petrov");
        Student petya = new Student(3, "Petya", "Ivanov");
        Student kolya = new Student(4, "Kolya", "Sidorov");
        Student masha =  new Student(5, "Masha", "Lopuhova");
        List<Student> studentList = new ArrayList<>();
        studentList.add(vasya);
        studentList.add(petya);
        studentList.add(kolya);
        studentList.add(masha);
        createMapFromList(studentList);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        printStudents(studentList, 4);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        sortStudent(studentList);
    }


    public static Map<String, Student> createMapFromList(List<Student> students) {
        Map<String, Student> stringStudentMap = new HashMap<>();
        for (int i = 0; i < students.size() ; i++) {
            stringStudentMap.put(students.get(i).getFirstName() + " " + students.get(i).getLastName() + " ", students.get(i));
        }
        for (Map.Entry entry : stringStudentMap.entrySet()) {
            System.out.println(entry);
        }
        return stringStudentMap;
    }

    public static void printStudents(List<Student> students, int course){
        Iterator<Student> iterator = students.iterator();
        while(iterator.hasNext()){
            Student st = iterator.next();
            if (st.getCourse() == course)
                System.out.println(st);
        }
    }

    public static List<Student> sortStudent(List<Student>  students){
        Collections.sort(students, (a,b) -> a.getFirstName().compareTo(b.getFirstName()));
        for (Student student : students) {
            System.out.println(student);
        }
        return students;
    }


}
