package ua.org.oa.makhnonosove.Practice4;

/**
 * Created by Евгений on 24.04.2016.
 */
public class Student {
    private String firstName;
    private String lastName;
    private int course;

    public Student(int course, String firstName, String lastName) {
        setCourse(course);
        setFirstName(firstName);
        setLastName(lastName);
    }

    public Student() {
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (getCourse() != student.getCourse()) return false;
        if (getFirstName() != null ? !getFirstName().equals(student.getFirstName()) : student.getFirstName() != null)
            return false;
        return !(getLastName() != null ? !getLastName().equals(student.getLastName()) : student.getLastName() != null);

    }

    @Override
    public int hashCode() {
        int result = getFirstName() != null ? getFirstName().hashCode() : 0;
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + getCourse();
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(" Student: ");
        sb.append("firstName - '").append(firstName).append('\'');
        sb.append(", lastName - '").append(lastName).append('\'');
        sb.append(", course - ").append(course);
        return sb.toString();
    }

}
