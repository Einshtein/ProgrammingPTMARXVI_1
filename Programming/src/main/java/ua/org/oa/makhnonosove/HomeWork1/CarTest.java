package ua.org.oa.makhnonosove.HomeWork1;

/**
 * Created by Евгений on 08.04.2016.
 */
 class Car {
    private String name;
    private int tank;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTank() {
        return tank;
    }

    public void setTank(int tank) {
        this.tank = tank;
    }

    public Car(String name, int tank) {
        setName(name);
        setTank(tank);
    }
}

public class CarTest {
public static void main(String[]args){
         Car car1 = new Car("VW", 50){
             @Override
             public boolean equals(Object obj) {
                 return super.equals(obj);
             }

             @Override
             public String toString() {
                 final StringBuilder sb = new StringBuilder("Марка: " + getName() + ", Объем бака " + getTank());
                 return sb.toString();
             }
         };
         Car car2 = new Car("Mersedes", 60){
             @Override
             public boolean equals(Object obj) {
                 return super.equals(obj);
             }

             @Override
             public String toString() {
                 final StringBuilder sb = new StringBuilder("Объем бака " + getTank() +  ", Марка: " + getName());
                 return sb.toString();
             }
         };
    System.out.println(car1);
    System.out.println(car2);
    }
 }
