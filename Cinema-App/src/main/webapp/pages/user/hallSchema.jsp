<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../common/Header.jsp"></jsp:include>
<div align="center" style="width: 1100px;">
    <h3>Cхема зала:</h3>
    <div style="padding-bottom: 10px;">
    <ul class="header">
        <li style="width: 12%; float: left; margin-left: 114px; ">Ряд</li>
    </ul>
    <c:forEach var="counter" begin="1" end="${seat_number}" varStatus="status">
        <ul class="header">
            <c:choose>
                <c:when test="${status.count <= seat_number}">
                    <li style="width: 27px; float: left; margin-left: 4px;">${counter}</li>
                </c:when>
                <%--<c:otherwise>--%>
                    <%--<li style="width: 27px;">${counter}</li>--%>
                <%--</c:otherwise>--%>
            </c:choose>
        </ul>
    </c:forEach>
    </div>
    <form method="get" action="${pageContext.servletContext.contextPath}/buy">
    <c:forEach var="row" items="${rowDTO}">
        <ul class="content">
            <li style="width: 12%;">${row.rowNumber}</li>
                <c:forEach var="counter" begin="1" end="${row.seatNumber}" >
                    <c:set var="id" value="${row.rowNumber}${counter}"></c:set>
                    <label>
                        <li style="width: 27px;">
                   <input type= "checkbox" name="ticket${row.rowNumber}${counter}" value="${row.rowNumber} ${counter}" id="${id}"
                    <c:forEach var="liId" items="${idList}">
                    <c:if test="${id eq liId}">
                          disabled</c:if>
                    </c:forEach>
                   /><span></span>

                    <%--${counter}--%>
                        </li>
                    </label>
                </c:forEach>
        </ul>
    </c:forEach>
        <input type="submit" value="Купить выбранные билеты" onClick="changeState('order_result','visible')">
        <input type="hidden" name="session_id" value="${session_id}">
    </form>

    <script language="javascript">

        function changeState(layerRef,state) {
            eval("document.all"+"['"+layerRef+"'].style.visibility='" +state+"'");
        }
    </script>
    <span id="order_result" style="visibility:hidden;">
<div style="position:relative;  width:400px; top:40%; border-top:1px solid white; border-left:1px solid white; border-right:1px solid white;
border-top-left-radius:5px; border-top-right-radius:5px; ">
    <center><font color="white" size="5">Ваш заказ успешно совершен!</font></center>
    <center><font color="white" size="3">Информация о заказе направлена на Ваш e-mail.</font></center>
</div>
<div style="position:relative; width:400px; top:40%; border-bottom:1px solid white; border-left:1px solid white;
border-right:1px solid white; border-bottom-left-radius:5px; border-bottom-right-radius:5px;">
    <center><font color="white">Перейти к </font><a href="${pageContext.servletContext.contextPath}/orders" style="color:white; text-decoration:underline;">заказам</a></center>
</div>
</span>
</div>
<jsp:include page="../common/Footer.jsp"></jsp:include>
