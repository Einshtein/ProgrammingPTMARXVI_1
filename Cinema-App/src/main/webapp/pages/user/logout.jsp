<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/Header.jsp"></jsp:include>
<%
    HttpSession newsession = request.getSession(false);
    if (newsession != null && request.getSession().getAttribute("user")!=null)
    {
        newsession.invalidate();
        out.println("<div align = 'center'><h1><font color=\"Red\">Вы успешно разлогинились</font></h1><br>Для входа обновите страницу.</div>");
%>
<%    }
    else {
        out.println("<div align = 'center'><h1><font color=\"Red\">Вы не залогинены! Введите логин и пароль или зарегистрируйтесь!</font></h1></div>");

    }
%>