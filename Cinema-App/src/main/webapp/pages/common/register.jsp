<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="Header.jsp"></jsp:include>
<form action="${pageContext.servletContext.contextPath}/registration" method="post" id="register_form" name="register_form" accept-charset="utf-8">
    <center><small>Звездочками отмечены обязательные поля</small></center>
    <center><font color="Red"><c:out value="${sessionScope.message}"/></font></center>
    <div style="width:50%; float:left; text-align:right; margin-top:10px;"><i>Фамилия: </i></div>
    <div style="text-align:left; margin-top:10px;"><input type="text" class="text" name="surname" style="width:200px;"  placeholder="Введите свою фамилию" autofocus/>
        *</div>
    <div style="width:50%; float:left; text-align:right; margin-top:5px;"><i>Имя: </i></div>
    <div style="text-align:left; margin-top:5px;"><input type="text" class="text" name="name" style="width:200px;"  placeholder="Введите свое имя"/>
        *</div>
    <div style="width:50%; float:left; text-align:right; margin-top:5px;"><i>E-mail: </i></div>
    <div style="text-align:left; margin-top:5px;"><input type="text" class="text" name="email" style="width:200px;" placeholder="Введите e-mail"/>
        *</div>
    <div style="width:50%; float:left; text-align:right; margin-top:5px;"><i>Дата рождения: </i></div>
    <div style="text-align:left; margin-top:5px;"><input type="text" class="text" name="birthday" style="width:200px;" placeholder="Введите дату рождения"/>
        *</div>
    <div style="width:50%; float:left; text-align:right; margin-top:5px;"><i>Пол: </i></div>
    <div style="text-align:left; margin-top:5px;"><input type="text" class="text" name="sex" style="width:200px;" placeholder="Введите пол"/>
        *</div>
    <div style="width:50%; float:left; text-align:right; margin-top:5px;"><i>Логин: </i></div>
    <div style="text-align:left; margin-top:5px;"><input type="text" class="text" name="login" style="width:200px;"  placeholder="Введите логин"/>
        *</div>
    <div style="width:50%; float:left; text-align:right; margin-top:5px;"><i>Пароль: </i></div>
    <div style="text-align:left; margin-top:5px;"><input type="password" class="text" name="password" style="width:200px;"  placeholder="Введите пароль"/>
        *</div>
    <div style="width:50%; text-align:right; margin-top:5px;">
        <div style="position:relative;">
            <div class="submit_reg"><button type="submit"><img src="../images/send.gif" alt="Отправить"/></button></div>
        </div>
    </div>
</form>

<jsp:include page="Footer.jsp"></jsp:include>

