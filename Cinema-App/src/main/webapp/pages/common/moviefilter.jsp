<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="Header.jsp"></jsp:include>
<c:if test="${fn:length(movieByTitle) eq 0 and fn:length(movieListByStartDate) eq 0 and fn:length(movieListByEndDate) eq 0}">
    <div align="center"><h2>${message}</h2></div>
</c:if>
<c:if test="${fn:length(movieByTitle) ne 0 or fn:length(movieListByStartDate) ne 0 or fn:length(movieListByEndDate) ne 0}">
<div align="center">
    <ul class="header">
        <li style="width: 12%;">
            Название</li>
        <li style="width: 12%;">
            Длительность</li>
        <li style="width: 12%;">
            Жанр</li>
        <li style="width: 12%;">
            Описание</li>
        <li style="width: 12%;">
            Рейтинг</li>
        <li style="width: 12%;">
            Дата начала проката</li>
        <li style="width: 12%;">
            Дата конца проката</li>
    </ul>
    <c:forEach var="mov" items="${movieListByStartDate}">
        <ul class="content">
            <li style="width: 12%;">
                <a href="${pageContext.servletContext.contextPath}/movie?id=${mov.id}">${mov.title}</a>
            </li>
            <li style="width: 12%;">${mov.duration}</li>
            <li style="width: 12%;">${mov.genre}</li>
            <li style="width: 12%;">${mov.description}</li>
            <li style="width: 12%;">${mov.rating}</li>
            <li style="width: 12%;">${mov.start}</li>
            <li style="width: 12%;">${mov.end}</li>
        </ul>
    </c:forEach>
    <c:forEach var="mov" items="${movieListByEndDate}">
        <ul class="content">
            <li style="width: 12%;">
                <a href="${pageContext.servletContext.contextPath}/movie?id=${mov.id}">${mov.title}</a>
            </li>
            <li style="width: 12%;">${mov.duration}</li>
            <li style="width: 12%;">${mov.genre}</li>
            <li style="width: 12%;">${mov.description}</li>
            <li style="width: 12%;">${mov.rating}</li>
            <li style="width: 12%;">${mov.start}</li>
            <li style="width: 12%;">${mov.end}</li>
        </ul>
    </c:forEach>
    <c:forEach var="mov" items="${movieByTitle}">
        <ul class="content">
            <li style="width: 12%;">
                <a href="${pageContext.servletContext.contextPath}/movie?id=${mov.id}">${mov.title}</a>
            </li>
            <li style="width: 12%;">${mov.duration}</li>
            <li style="width: 12%;">${mov.genre}</li>
            <li style="width: 12%;">${mov.description}</li>
            <li style="width: 12%;">${mov.rating}</li>
            <li style="width: 12%;">${mov.start}</li>
            <li style="width: 12%;">${mov.end}</li>
        </ul>
    </c:forEach>
</div>
</c:if>
<jsp:include page="Footer.jsp"></jsp:include>