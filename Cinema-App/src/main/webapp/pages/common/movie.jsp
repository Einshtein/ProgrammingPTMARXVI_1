<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="Header.jsp"></jsp:include>
<div align="center">
    <h3>Вот информация по запрашиваемому Вами фильму:</h3>

    <div class="show_info">
        <i>Название: </i>${movie.title}
    </div>
    <div class="show_info">
        <i>Жанр: </i> ${movie.genre}
    </div>
    <div class="show_info">
        <i>Описание: </i>${movie.description}
    </div>
    <div class="show_info">
        <i>Рейтинг: </i>${movie.rating}
    </div>
    <div class="show_info">
        <i>Дата начала проката: </i>${movie.start}
    </div>
    <div class="show_info">
        <i>Дата конца проката: </i>${movie.end}
    </div>
    <div class="show_info">
        <a href="${pageContext.servletContext.contextPath}/session?movie_id=${movie.id}">Посмотреть сеансы</a>
    </div>

</div>
<jsp:include page="Footer.jsp"></jsp:include>