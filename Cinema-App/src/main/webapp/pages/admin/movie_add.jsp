<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../common/Header.jsp"></jsp:include>
<div align="center">
    <form action="${pageContext.servletContext.contextPath}/add" method="get" id="movie_add" name="test" accept-charset="utf-8">
        <center>Добавить фильм:</center>
        <div style="width:50%; float:left; text-align:right; margin-top:5px;"><i>Название: </i></div>
        <div style="text-align:left; margin-top:5px;"><input type="text" class="text" name="title" style="width:200px;"  placeholder="" autofocus/>
            </div>
        <div style="width:50%; float:left; text-align:right; margin-top:5px;"><i>Длительность: </i></div>
        <div style="text-align:left; margin-top:5px;"><input type="text" class="text" name="duration" style="width:200px;"  placeholder=""/>
            </div>
        <div style="width:50%; float:left; text-align:right; margin-top:5px;"><i>Жанр: </i></div>
        <div style="text-align:left; margin-top:5px;"><input type="text" class="text" name="genre" style="width:200px;"  placeholder=""/>
            </div>
        <div style="width:50%; float:left; text-align:right; margin-top:5px;"><i>Описание: </i></div>
        <div style="text-align:left; margin-top:5px;"><input type="text" class="text" name="description" style="width:200px;" placeholder=""/>
            </div>
        <div style="width:50%; float:left; text-align:right; margin-top:5px;"><i>Рейтинг: </i></div>
        <div style="text-align:left; margin-top:5px;"><input type="text" class="text" name="rating" style="width:200px;" placeholder=""/>
            </div>
        <div style="width:50%; float:left; text-align:right; margin-top:5px;"><i>Дата начала проката: </i></div>
        <div style="text-align:left; margin-top:5px;"><input type="text" class="text" name="start" style="width:200px;" placeholder=""/>
            </div>
        <div style="width:50%; float:left; text-align:right; margin-top:5px;"><i>Дата конца проката: </i></div>
        <div style="text-align:left; margin-top:5px;"><input type="text" class="text" name="end" style="width:200px;"  placeholder=""/>
            </div>
            <div style="position:relative; margin-top: 20px;">
                <input type="submit" value="Добавить"/>
            </div>

    </form>
</div>
<jsp:include page="../common/Footer.jsp"></jsp:include>
