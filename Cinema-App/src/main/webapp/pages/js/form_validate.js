$(document).ready(function(){

    $("#register_form").validate({

        rules:{
            login:{
                required: true,
                minlength: 4,
                maxlength: 16,
            },

           password:{
                required: true,
                minlength: 6,
                maxlength: 16,
            },

           surname:{
               required: true,
           },

           name:{
               required: true,
           },

           email:{
               required: true,
               email:true,
               pattern:'\\w+\\@\\w+\\.\\w{2,3}',
           },
           birthday:{
               required: true,
               pattern:'\\d{0,4}\\-\\d{0,2}\\-\\d{0,2}',
           },

           sex:{
               required: true,
           },
       },
       
       messages:{
        
            login:{
                required: "This field is required!",
            },

           password:{
                required: "This field is required!",
            },

           name:{
               required: "This field is required!",
           },

           surname:{
               required: "This field is required!",
           },

           email:{
               required: "This field is required!",
           },

           sex:{
               required: "This field is required!",
           },
           birthday:{
               required: "This field is required!",
               pattern:"Date format is YYYY-MM-DD",
           },
       }
        
    });


    $("#movie_start_date_form").validate({
        rules: {
            start: {
                required: true,
                pattern:'\\d{0,4}\\-\\d{0,2}\\-\\d{0,2}',
            },
        },
        messages:{
            start:{
                pattern:"Date format is YYYY-MM-DD",
            },
        }

    });

    $("#movie_end_date_form").validate({
        rules: {
            end: {
                required: true,
                pattern:'\\d{0,4}\\-\\d{0,2}\\-\\d{0,2}',
            },
        },

        messages:{
            end:{
                pattern:"Date format is YYYY-MM-DD",
            },
        }

    });

    $("#movie_add").validate({

        rules:{
            title:{
                required: true,
            },

            duration:{
                required: true,
                digits:true,
            },

            genre:{
                required: true,
            },

            description:{
                required: true,
            },

            rating:{
                required: true,
            },
            start:{
                required: true,
                date:true,
            },

            end:{
                required: true,
                date:true,
            },
        },

        messages:{
            start:{
                pattern:"Date format is YYYY-MM-DD",
            },
            end:{
                pattern:"Date format is YYYY-MM-DD",
            },
        }
    });


    $("#hall_add").validate({

        rules:{
            name:{
                required: true,
            },
        },
        messages:{

        }

    });

    $("#session_add").validate({

        rules:{
            time:{
                required: true,
                pattern:'\\d{0,2}\\:\\d{0,2}\\:\\d{0,2}',
            },

            date:{
                required: true,
                date:true,
            },

            price:{
                required: true,
                digits:true,
            },
        },

        messages:{
            time:{
                pattern:"Time format is HH:MM:SS",
            },

            date:{
                date:"Date format is YYYY-MM-DD",
            },
        }

    });

}); //end of ready