var main = function() {
    $('.article_admin').click(function() {
        $('.article_admin').removeClass('current');
        $('.description').hide();

        $(this).addClass('current');
        $(this).children('.description').show();
    });

    $(document).keypress(function(event) {
        if(event.which === 111) {
            $('.description').hide();

            $('.current').children('.description').show();
        }

        else if(event.which === 110) {
            var currentArticle = $('.current');
            var nextArticle = currentArticle.next();

            currentArticle.removeClass('current');
            nextArticle.addClass('current');
        }
    });
}

$(document).ready(main);