package model;


import java.time.LocalDate;

public class Movie extends Entity<Integer> {

    private String title;
    private String description;
    private int duration;
    private Double rating;
    private String genre;
    private LocalDate start;
    private LocalDate end;

    public Movie(String title, String description, int duration, LocalDate start, LocalDate end, String genre, Double rating) {
        this.description = description;
        this.duration = duration;
        this.end = end;
        this.genre = genre;
        this.rating = rating;
        this.start = start;
        this.title = title;
    }


    public Movie() {
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Movie movie = (Movie) o;

        if (getId() != movie.getId()) return false;
        if (getDuration() != movie.getDuration()) return false;
        if (getTitle() != null ? !getTitle().equals(movie.getTitle()) : movie.getTitle() != null) return false;
        if (getDescription() != null ? !getDescription().equals(movie.getDescription()) : movie.getDescription() != null)
            return false;
        if (getRating() != null ? !getRating().equals(movie.getRating()) : movie.getRating() != null) return false;
        if (getGenre() != null ? !getGenre().equals(movie.getGenre()) : movie.getGenre() != null) return false;
        if (getStart() != null ? !getStart().equals(movie.getStart()) : movie.getStart() != null) return false;
        return !(getEnd() != null ? !getEnd().equals(movie.getEnd()) : movie.getEnd() != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getId();
        result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + getDuration();
        result = 31 * result + (getRating() != null ? getRating().hashCode() : 0);
        result = 31 * result + (getGenre() != null ? getGenre().hashCode() : 0);
        result = 31 * result + (getStart() != null ? getStart().hashCode() : 0);
        result = 31 * result + (getEnd() != null ? getEnd().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MovieDTO: ");
        sb.append("description = '").append(description).append('\'');
        sb.append(", id = ").append(getId());
        sb.append(", title = '").append(title).append('\'');
        sb.append(", duration = ").append(duration);
        sb.append(", rating = ").append(rating);
        sb.append(", genre = '").append(genre).append('\'');
        sb.append(", start = ").append(start);
        sb.append(", end = ").append(end).append("\n");
        return sb.toString();
    }
}
