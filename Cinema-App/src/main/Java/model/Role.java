package model;

public class Role extends Entity<Integer> {
    private String name;

    public Role(String name) {
        this.name = name;
    }

    public Role() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        return !(getName() != null ? !getName().equals(role.getName()) : role.getName() != null);

    }

    @Override
    public int hashCode() {
        return getName() != null ? getName().hashCode() : 0;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RoleDTO: ");
        sb.append("name = '").append(name).append('\'');
        sb.append(", id = ").append(getId());
        return sb.toString();
    }
}
