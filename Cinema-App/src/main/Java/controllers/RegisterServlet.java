package controllers;

import dto.RoleDTO;
import dto.UserDTO;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.List;


@WebServlet(name = "RegisterServlet", urlPatterns={"/registration"})
public class RegisterServlet extends HttpServlet {
    private static final int PASSWORD_LENGTH = 6;
    private static final int LOGIN_LENGTH = 6;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //получаем из запроса данные нового юзера
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String email = request.getParameter("email");
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String birthday = request.getParameter("birthday");
        String sex = request.getParameter("sex");
        //проверка на наличие юзера в базе
        List<UserDTO> userList = UserServiceImpl.getInstance().getAll();
        boolean isUser = false;
        for (int i = 0; i < userList.size(); i++) {
            if(!login.equals(userList.get(i).getLogin())){
                continue;
            }
            if(login.equals(userList.get(i).getLogin())){
                isUser = true;
            }
        }
        if (isUser) {
            //если такой уже есть - отклоняем
            request.getSession().setAttribute("message", "Такой пользователь уже существует!");
            response.sendRedirect(request.getContextPath() + "/pages/common/register.jsp");
        }
        else {
            //если нет - вносим в базу
            UserDTO user = new UserDTO(name, surname, login, password, new RoleDTO("user"), Date.valueOf(birthday).toLocalDate(), email, sex);
            UserServiceImpl.getInstance().upload(user);
            request.getRequestDispatcher("pages/common/register_true.jsp").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
