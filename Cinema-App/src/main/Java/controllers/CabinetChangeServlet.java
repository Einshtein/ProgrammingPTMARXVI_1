package controllers;

import dto.UserDTO;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;

@WebServlet(name = "CabinetChangeServlet", urlPatterns={"/cabinet"})
public class CabinetChangeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //получаем из запроса все данные
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String email = request.getParameter("email");
        String birthday = request.getParameter("birthday");
        String sex = request.getParameter("sex");
        UserDTO user = (UserDTO) request.getSession().getAttribute("user");
        //сетим изменения
        user.setName(name);
        user.setSurname(surname);
        user.setSex(sex);
        user.setBirthday(Date.valueOf(birthday).toLocalDate());
        user.setEmail(email);
        //обновляем юзера в базе
        UserServiceImpl.getInstance().update(user);
        request.getSession().setAttribute("message", "успешно изменены!");
        request.getRequestDispatcher("pages/user/cabinet.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
