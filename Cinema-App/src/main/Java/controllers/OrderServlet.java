package controllers;

import dto.TicketDTO;
import dto.UserDTO;
import service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet(name = "OrderServlet", urlPatterns={"/orders"})
public class OrderServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //выводим из базы заказы по id юзера
        UserDTO user = (UserDTO) request.getSession().getAttribute("user");
            if(request.getSession().getAttribute("user") != null) {
                List<TicketDTO> ticketByUser = TicketServiceImpl.getInstance().getTicketByUser(user.getId());
                if(ticketByUser.size() == 0){
                    request.getSession().setAttribute("message", "У Вас нет заказов!");
                }
                request.setAttribute("ticketByUser", ticketByUser);
            request.getRequestDispatcher("pages/user/orders.jsp").forward(request,response);
        }
        else{
            request.getSession().setAttribute("message", "Вы не зарегистрированы!");
            response.sendRedirect(request.getContextPath() + "/login.jsp");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }


}
