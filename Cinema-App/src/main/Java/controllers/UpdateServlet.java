package controllers;

import dto.HallDTO;
import dto.MovieDTO;
import dto.SessionDTO;
import service.impl.HallServiceImpl;
import service.impl.MovieServiceImpl;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Time;
import java.time.LocalDate;

@WebServlet(name = "UpdateServlet", urlPatterns = {"/update"})
public class UpdateServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String movie_id = request.getParameter("movie_id");
        if (movie_id != null) {
            //получаем данные из запроса
            String title = request.getParameter("title");
            String duration = request.getParameter("duration");
            String genre = request.getParameter("genre");
            String description = request.getParameter("description");
            String rating = request.getParameter("rating");
            String start_date = request.getParameter("start_date");
            String end_date = request.getParameter("end_date");
            //получаем фильм из базы по id
            MovieDTO movie = MovieServiceImpl.getInstance().getById(Integer.valueOf(movie_id));
            movie.setDescription(description);
            movie.setDuration(Integer.valueOf(duration));
            movie.setTitle(title);
            movie.setEnd(LocalDate.parse(end_date));
            movie.setStart(LocalDate.parse(start_date));
            movie.setGenre(genre);
            movie.setRating(Double.valueOf(rating));
            request.setAttribute("movie", movie);
            //обновляем данные
            MovieServiceImpl.getInstance().update(movie);
            request.getRequestDispatcher("pages/admin/update.jsp").forward(request, response);
        }
        //получаем зал из базы по id
        String hall_id = request.getParameter("hall_id");
        if (hall_id != null) {
            HallDTO hall = HallServiceImpl.getInstance().getById(Integer.valueOf(hall_id));
            String name = request.getParameter("name");
            hall.setName(name);
            //обновляем данные
            HallServiceImpl.getInstance().update(hall);
            request.setAttribute("hall", hall);
            request.getRequestDispatcher("pages/admin/update.jsp").forward(request, response);
        }
        //получаем сессию из базы по id
        String session_id = request.getParameter("session_id");
        if (session_id != null) {
            SessionDTO session = SessionServiceImpl.getInstance().getById(Integer.valueOf(session_id));
            String time = request.getParameter("time");
            String date = request.getParameter("date");
            String price = request.getParameter("price");
            session.setDate(LocalDate.parse(date));
            session.setPrice(new BigDecimal(price));
            session.setSessionTime(Time.valueOf(time));
            //обновляем данные
            SessionServiceImpl.getInstance().update(session);
            request.setAttribute("session", session);
            request.getRequestDispatcher("pages/admin/update.jsp").forward(request, response);
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
