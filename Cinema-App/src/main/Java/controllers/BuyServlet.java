package controllers;

import dao.impl.TicketDaoImpl;
import dto.SessionDTO;
import dto.TicketDTO;
import dto.UserDTO;
import model.Ticket;
import service.impl.SessionServiceImpl;
import service.impl.TicketServiceImpl;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;


@WebServlet(name = "BuyServlet", urlPatterns={"/buy"})
public class BuyServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //получаем id сессиии и юзера
        TicketDaoImpl ticketDaoImpl = new TicketDaoImpl(Ticket.class);
        Integer sessionId = Integer.valueOf(request.getParameter("session_id"));
        UserDTO user = (UserDTO) request.getSession().getAttribute("user");
        SessionDTO sessionDTO = SessionServiceImpl.getInstance().getById(sessionId);
        //получаем из запроса номер ряда и места
        Enumeration parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
                String paramName = (String) parameterNames.nextElement();
            if(paramName.startsWith("ticket")) {
                String[] paramValues = request.getParameterValues(paramName);
                int row = 0;
                int seat = 0;
                for (int i = 0; i < paramValues.length; i++) {
                        row = Integer.valueOf(paramValues[i].substring(0, paramValues[i].indexOf(" ")));
                        seat = Integer.valueOf(paramValues[i].substring(paramValues[i].indexOf(" ")+1, paramValues[i].length()));
                    TicketDTO ticketDTO = new TicketDTO(row, seat, true, user, sessionDTO);
                    //добавляем в базу купленный билет для каждого места
                    TicketServiceImpl.getInstance().upload(ticketDTO);
                    //отправка почты
                    String  subject1 = "Спасибо за Ваш заказ на сайте \"Киноленд\" №" + user.getId()+row+seat;
                    String  text1 = "<h1>Спасибо за Ваш заказ!</h1><h2>Информация о заказе:<br>Фильм: " +sessionDTO.getMovie().getTitle()
                            +"<br>Сеанс: "+ sessionDTO.getSessionTime()
                            +"<br>Дата: "+ sessionDTO.getDate()
                            +"<br>Ваш ряд: " + row + "<br>Ваше место: " + seat +"</h2>";
                    String  emaiTo = ticketDTO.getUser().getEmail();
                    mailSend(subject1, text1, emaiTo);
                }
            }
        }

        try {
            Thread.currentThread().sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher("pages/user/buy.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    public static void mailSend(String subject, String text, String emailTo)
            throws IOException {
        String mailer = "sendhtml";
        final String myMail = "eastelshop@gmail.com";
        final String myName = "eastelshop@gmail.com";
        final String username = "eastelshop@gmail.com";
        final String password = "owervhelming";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.googlemail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.mime.charset", "UTF-8");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            MimeMessage message = new MimeMessage(session);
            message.setHeader("Content-Type", "text/html; charset=UTF-8");
            message.setFrom(new InternetAddress(myMail, myName));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(emailTo));
            message.setSubject(subject, "UTF-8");
            message.setContent(text ,"text/html; charset=\"UTF-8\"");
            Transport.send(message);


        } catch (MessagingException e) {
            throw new RuntimeException( e );
        }

    }
}
