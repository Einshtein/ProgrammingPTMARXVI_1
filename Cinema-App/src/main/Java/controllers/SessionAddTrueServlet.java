package controllers;

import dto.HallDTO;
import dto.MovieDTO;
import dto.SessionDTO;
import service.impl.HallServiceImpl;
import service.impl.MovieServiceImpl;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Time;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by Евгений on 05.07.2016.
 */
@WebServlet(name = "SessionAddTrueServlet", urlPatterns={"/session_add_true"})
public class SessionAddTrueServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //получаем данные из сеанса
        String hall_name = request.getParameter("hall_name");
        String movie_name = request.getParameter("movie_name");
        String date = request.getParameter("date");
        String price = request.getParameter("price");
        String time = request.getParameter("time");
        //получаем муви и холл по именам
        if(hall_name != null && movie_name != null && date != null && price != null && time !=null) {
        List<MovieDTO> movie = MovieServiceImpl.getInstance().getMovieByTitle(movie_name);
        HallDTO hall = HallServiceImpl.getInstance().getHallByName(hall_name);
        //добавляем сеанс
        SessionDTO session = new SessionDTO(hall, movie.get(0), new BigDecimal(price), Time.valueOf(time), LocalDate.parse(date));
        request.setAttribute("session", session);
        SessionServiceImpl.getInstance().upload(session);
        request.getRequestDispatcher("pages/admin/add_true.jsp").forward(request, response);
            }
        }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
