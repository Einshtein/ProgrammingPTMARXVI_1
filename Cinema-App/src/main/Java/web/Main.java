package web;

import dao.impl.*;
import model.Hall;
import model.Movie;
import model.Row;
import model.Session;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;


public class Main {
    public static void main(String[] args) throws UnsupportedEncodingException, NoSuchAlgorithmException {
//
//        System.out.println(PropertyHolder.getInstance().getJdbcUrl());
//
//        Connection connection = DataSource.getInstance().getConnection();


        CrudDAO<Movie> movieDao = new MovieDaoImpl(Movie.class);
        MovieDaoImpl movieDaoImpl = new MovieDaoImpl(Movie.class);

        CrudDAO<Hall> hallDao = new HallDaoImpl(Hall.class);
        HallDaoImpl hallDaoImpl = new HallDaoImpl(Hall.class);
        CrudDAO<Row> rowDAO = new RowDaoimpl(Row.class);
        SessionDaoImpl sessionDaoImpl = new SessionDaoImpl(Session.class);
//            System.out.println(movieDao.getById(1));

        Movie movie = new Movie("wwew", "ee", 120, LocalDate.of(2016, 6, 6), LocalDate.of(2016, 6, 7), "апвапв", 10.0);
        Movie movie1 = new Movie("Heavy New Year", "dfgfg", 120, LocalDate.of(2016, 6, 6), LocalDate.of(2016, 6, 7), "horror", 10.0);
        movieDao.upload(movie);
//        movieDao.getAll();
//        Movie movie2 = movieDao.getById(6);
//        movie2.setDescription("Vasya");
//        movieDao.update(movie2);
//        movieDaoImpl.updateMovieTitleById("Happy New Year", 4);
//        System.out.println(movieDaoImpl.getMovieByTitle("Happy New Year"));
//        System.out.println(movieDaoImpl.getMovieByStartDate("2016-01-01"));
//        System.out.println("~~~~~~~~~~~~~~~");
//        movieDaoImpl.getMovieByEndDate("2016-05-11");
//        List<MovieDTO> movieListByStartDate = MovieServiceImpl.getInstance().getMovieByStartDate("2016-06-06");
//        System.out.println(movieListByStartDate);
//         movieDao.delete(1);
//
//        Hall hall = new Hall("Blue");
//////        hallDaoImpl.upload(hall);
////        System.out.println(hallDaoImpl.getAll());
////        System.out.println(hallDaoImpl.getHallByName("Blue"));
//        Session session = new Session(hallDao.getById(1), movieDao.getById(6), new BigDecimal(100.00), new Time (15,10,00), LocalDate.of(2016,10,10));
////        sessionDaoImpl.upload(session);
////        System.out.println(sessionDaoImpl.getAll());
//        System.out.println(rowDAO.getAll());
//
//
//        MovieDTO movieDTO = new MovieDTO("Heavy New Year_2", "Котэ", 120, LocalDate.of(2016,6,6), LocalDate.of(2016,6,7), "animal_horror", 10.0);
//
//        MovieServiceImpl.getInstance().upload(movieDTO);
//
//        System.out.println(MovieServiceImpl.getInstance().getAll());
////
//        UserDTO userDTO = UserServiceImpl.getInstance().getUserByLogin("einshtein");
//        System.out.println(userDTO);
//        System.out.println(userDTO.getPassword());


//
//        UserDTO userDTO = new UserDTO("kotya", "koteyka", "kot", "11221", new RoleDTO("user"), Date.valueOf("2016-05-05").toLocalDate(), "sdfd@sdf.sdf", "male");
//        UserServiceImpl.getInstance().upload(userDTO);
//        UserDTO userDTO =  UserServiceImpl.getInstance().getById(15);
//        userDTO.setName("cat");
//        UserServiceImpl.getInstance().update(userDTO);
//
//        UserDaoImpl userDaoImpl = new UserDaoImpl(User.class);
//        User user = new User("kotya", "koteyka", "kote", "1111", new Role("user"), LocalDate.of(2016,06,06), "sdfd@sdf.sdf", "male");
//        userDaoImpl.upload(user);
//        System.out.println(userDaoImpl.getAll());
//        String pass = userDaoImpl.getUserByName("vasya").getPassword();
////        String password =  userDaoImpl.encryptPassword("123456");
//        System.out.println(pass);
//        System.out.println(password);
////

//        List<RowDTO> rowDTO = RowServiceImpl.getInstance().getRowByHallId(Integer.valueOf(1));
////        System.out.println(rowDTO);
//        List<TicketDTO> ticketDTOList = TicketServiceImpl.getInstance().getTicketBySession(Integer.valueOf(3));
////        for (TicketDTO ticketDTO : ticketDTOList) {
////            System.out.println(ticketDTO.getRow() + " " + ticketDTO.getSeatNumber());
//////        }
//        String visibility = null;
//        Integer liId = 0;
//        for (TicketDTO ticketDTO : ticketDTOList) {
//            for (int i = 0; i < rowDTO.size() ; i++) {
//                String row = String.valueOf(i + 1);
//                for (int j = 1; j <= rowDTO.get(i).getSeatNumber(); j++) {
//
//                    if ((ticketDTO.getRow() == rowDTO.get(i).getRowNumber()) && (ticketDTO.getSeatNumber() == j)) {
//                        String seat = String.valueOf(j);
////                        visibility = "hidden";
////                        System.out.println(visibility);
//                        String id = row.concat(seat);
//                        liId = Integer.valueOf(id);
//                        System.out.print(liId + " ");
//
//                    }
////                    else {
////                        visibility = "visible";
//////                        System.out.println(visibility);
////                        String id = String.valueOf(ticketDTO.getRow()).concat(String.valueOf(j));
////                        liId = Integer.valueOf(id);
////                        System.out.println(liId);
////                    }
//                }
//            }
//                }
////            }
////
////        for (int i = 1; i <= 10 ; i++) {
////            System.out.print(i);
////               for (int j = 1; j <= 20; j++) {
////                   System.out.print(" ");
////                   System.out.print(j);
////            }
////            System.out.println();
////            }

//        System.out.println(Main.encryptPassword("1234"));
    }

    public static String encryptPassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {

        final StringBuilder sbMd5Hash = new StringBuilder();
        final MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(password.getBytes("UTF-8"));

        final byte data[] = m.digest();

        for (byte element : data) {
            sbMd5Hash.append(Character.forDigit((element >> 4) & 0xf, 16));
            sbMd5Hash.append(Character.forDigit(element & 0xf, 16));
        }

        return sbMd5Hash.toString();
    }
}