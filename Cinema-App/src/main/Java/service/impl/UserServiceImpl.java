package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dao.impl.UserDaoImpl;
import dto.UserDTO;
import mapper.BeanMapper;
import model.User;
import service.api.Service;

import java.util.List;

public class UserServiceImpl implements Service<Integer, UserDTO> {

    private static UserServiceImpl service;
    private Dao<Integer, User> userDao;
    private BeanMapper beanMapper;
    private UserDaoImpl userDaoImpl;

    private UserServiceImpl() {
        userDao = DaoFactory.getInstance().getUserDao();
        beanMapper = BeanMapper.getInstance();
        userDaoImpl = new UserDaoImpl(User.class);
    }

    public static synchronized UserServiceImpl getInstance() {
        if (service == null) {
            service = new UserServiceImpl();
        }
        return service;
    }

    @Override
    public List<UserDTO> getAll() {
        List<User> users = userDao.getAll();
        List<UserDTO> userDTOs = beanMapper.listMapToList(users, UserDTO.class);
        return userDTOs;
    }

    @Override
    public void upload(UserDTO userDto) {
        User user = beanMapper.singleMapper(userDto, User.class);
            userDao.upload(user);
    }

    @Override
    public UserDTO getById(Integer id) {
        User user = userDao.getById(id);
        UserDTO userDTO = beanMapper.singleMapper(user, UserDTO.class);
        return userDTO;
    }

    //проверить
    @Override
    public void delete(Integer key) {
        userDao.delete(key);
    }

    //проверить
    @Override
    public void update(UserDTO userDto) {
        User user = beanMapper.singleMapper(userDto, User.class);
        userDao.update(user);
    }

    public UserDTO getUserByLogin (String login) {
        User user = userDaoImpl.getUserByLogin(login);
        UserDTO userDTO = beanMapper.singleMapper(user, UserDTO.class);
        return userDTO;
    }

}
