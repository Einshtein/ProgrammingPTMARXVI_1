<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Доска объявлений</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <link href="<c:url value="/css/my_profile_style.css" />" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="header_container">
    <div class="title" style="text-align: center;"><a href="${pageContext.servletContext.contextPath}/MyProfile/"><font face="Arial" size="9pt" ><i><b>Доска объявлений</b></i></font></a></div>
    <div class="menu_container" align="center">
        <ul class="menu">
            <li style="width:${(100/4)-1}%;">
                <a href="${pageContext.servletContext.contextPath}/login">Войти</a>
            </li>
        </ul>
    </div>
</div>
<div class="container">

        <h3>Объявления:</h3>
    <c:if test="${fn:length(advs) eq 0}">
        <div align="center"><h2>${message}</h2></div>
    </c:if>
    <c:if test="${fn:length(advs) ne 0}">
        <form class="form-inline" role="form" action="${pageContext.servletContext.contextPath}/search" method="get">
            <input type="text" class="form-control" name="pattern" placeholder="Поиск">
            <input type="submit" class="btn btn-default" value="Поиск">
        </form>


        <table class="table table-striped">
            <thead>
            <tr>
                <td><b>Фото</b></td>
                <td><b>Название</b></td>
                <td><b>Крат. описание</b></td>
                <td><b>Описание</b></td>
                <td><b>Тел.</b></td>
                <td><b>Цена</b></td>
            </tr>
            </thead>
                <c:forEach items="${advs}" var="adv">
                <tr>
                    <td><img height="40" width="40" src="${pageContext.servletContext.contextPath}/image/${adv.photo.id}" /></td>
                    <td>${adv.name}</td>
                    <td>${adv.shortDesc}</td>
                    <td>${adv.longDesc}</td>
                    <td>${adv.phone}</td>
                    <td>${adv.price}</td>
                </tr>
                </c:forEach>
        </table>
    </c:if>
</div>
</body>
</html>