<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="HF/Header.jsp"></jsp:include>
<html>
<head>
    <title>Р”РѕСЃРєР° РѕР±СЉСЏРІР»РµРЅРёР№</title>
</head>
<body>
<div class="container">
    <h3>Р”РѕР±СЂРѕ РїРѕР¶Р°Р»РѕРІР°С‚СЊ, ${user.name} ${user.surname}</h3>
    <c:if test="${fn:length(advs) eq 0}">
        <div align="center"><h2>${message}</h2></div>
    </c:if>
    <c:if test="${fn:length(advs) ne 0}">
    <h3>Р’Р°С€Рё РѕР±СЉСЏРІР»РµРЅРёСЏ:</h3>

    <form class="form-inline" role="form" action="${pageContext.servletContext.contextPath}/MyProfile/search" method="get">
        <input type="text" class="form-control" name="pattern" placeholder="РџРѕРёСЃРє">
        <input type="submit" class="btn btn-default" value="РџРѕРёСЃРє">
    </form>


    <table class="table table-striped">
        <thead>
        <tr>
            <td><b>Р’С‹Р±СЂР°С‚СЊ</b></td>
            <td><b>Р¤РѕС‚Рѕ</b></td>
            <td><b>РќР°Р·РІР°РЅРёРµ</b></td>
            <td><b>РљСЂР°С‚. РѕРїРёСЃР°РЅРёРµ</b></td>
            <td><b>РћРїРёСЃР°РЅРёРµ</b></td>
            <td><b>РўРµР».</b></td>
            <td><b>Р¦РµРЅР°</b></td>
            <td><b>Р”РµР№СЃС‚РІРёРµ</b></td>
        </tr>
        </thead>
        <form class="form-inline" role="form" action="${pageContext.servletContext.contextPath}/MyProfile/deleteSelected" method="get">
        <c:forEach items="${advs}" var="adv">
            <tr>
                <td><input type="checkbox" value="${adv.id}" name="idList" id="advDelete"></td>
                <td><img height="40" width="40" src="${pageContext.servletContext.contextPath}/MyProfile/image/${adv.photo.id}" /></td>
                <td>${adv.name}</td>
                <td>${adv.shortDesc}</td>
                <td>${adv.longDesc}</td>
                <td>${adv.phone}</td>
                <td>${adv.price}</td>
                <td><a href="${pageContext.servletContext.contextPath}/MyProfile/delete?id=${adv.id}">РЈРґР°Р»РёС‚СЊ</a></td>
            </tr>
        </c:forEach>
    </table>
    <input type="submit" class="btn btn-default delete_button" value="РЈРґР°Р»РёС‚СЊ РІС‹Р±СЂР°РЅС‹Рµ" style="float:left; margin-right:10px;">

    </form>
    </c:if>
    <form class="form-inline" role="form" action="${pageContext.servletContext.contextPath}/MyProfile/add_page" method="post">
        <input type="submit" class="btn btn-primary" value="Р”РѕР±Р°РІРёС‚СЊ РѕР±СЉСЏРІР»РµРЅРёРµ">
    </form>

</div>
</body>
</html>