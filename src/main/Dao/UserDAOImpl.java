package main.Dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import main.Model.Basket;
import main.Model.User;
import org.springframework.beans.factory.annotation.Autowired;

public class UserDAOImpl
  implements UserDAO
{
  @Autowired
  private EntityManager entityManager;
  
  public void add(User user)
  {
    try
    {
      this.entityManager.getTransaction().begin();
      this.entityManager.persist(user);
      this.entityManager.getTransaction().commit();
    }
    catch (Exception ex)
    {
      this.entityManager.getTransaction().rollback();
      ex.printStackTrace();
    }
  }
  
  public void delete(long id)
  {
    try
    {
      this.entityManager.getTransaction().begin();
      User user = (User)this.entityManager.find(User.class, Long.valueOf(id));
      this.entityManager.remove(user);
      this.entityManager.getTransaction().commit();
    }
    catch (Exception ex)
    {
      this.entityManager.getTransaction().rollback();
      ex.printStackTrace();
    }
  }
  
  public User getUserById(Integer id)
  {
    Query query = this.entityManager.createQuery("SELECT a FROM User a WHERE a.id = :id", Basket.class);
    query.setParameter("id", id);
    return (User)query.getResultList().get(0);
  }
  
  public List<User> getAllUsers()
  {
    Query query = this.entityManager.createQuery("SELECT a FROM User a", User.class);
    return query.getResultList();
  }
  
  public User getUserByLogin(String login)
  {
    Query query = this.entityManager.createQuery("SELECT a FROM User a WHERE a.login = :login", User.class);
    query.setParameter("login", login);
    return (User)query.getResultList().get(0);
  }
}
