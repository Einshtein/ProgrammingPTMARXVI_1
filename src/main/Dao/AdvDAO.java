package main.Dao;

import main.Model.Advertisement;
import main.Model.AdvertisementAll;
import main.Model.Basket;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public abstract interface AdvDAO
{
  public abstract List<AdvertisementAll> getAdv();
  
  public abstract List<AdvertisementAll> getAdv(String paramString);
  
  public abstract List<Advertisement> getAllAdv(long paramLong);
  
  public abstract List<Advertisement> getAllAdv(String paramString);
  
  public abstract List<Basket> getAdvFromBasket();
  
  public abstract List<Basket> getAllBasket(String paramString);
  
  public abstract void add(Advertisement paramAdvertisement);
  
  public abstract void delete(long paramLong, HttpServletRequest paramHttpServletRequest);
  
  public abstract void deleteByCheckbox(List<Long> paramList);
  
  public abstract void restore(long paramLong, HttpServletRequest paramHttpServletRequest);
  
  public abstract void restoreByCheckbox(List<Long> paramList);
  
  public abstract byte[] getPhoto(long paramLong);
  
  public abstract void truncate(long paramLong);
}
