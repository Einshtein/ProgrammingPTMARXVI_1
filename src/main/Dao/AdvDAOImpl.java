package main.Dao;

import main.Model.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AdvDAOImpl
  implements AdvDAO
{
  @Autowired
  private EntityManager entityManager;
  
  public List<AdvertisementAll> getAdv()
  {
    Query query = this.entityManager.createQuery("SELECT a FROM AdvertisementAll a", AdvertisementAll.class);
    return query.getResultList();
  }
  
  public List<AdvertisementAll> getAdv(String pattern)
  {
    Query query = this.entityManager.createQuery("SELECT a FROM AdvertisementAll a WHERE a.shortDesc LIKE :pattern", AdvertisementAll.class);
    query.setParameter("pattern", "%" + pattern + "%");
    return query.getResultList();
  }
  
  public List<Advertisement> getAllAdv(long id)
  {
    Query query = this.entityManager.createQuery("SELECT a FROM Advertisement a where a.user.id = :id", Advertisement.class);
    query.setParameter("id", Long.valueOf(id));
    return query.getResultList();
  }
  
  public List<Advertisement> getAllAdv(String pattern)
  {
    Query query = this.entityManager.createQuery("SELECT a FROM Advertisement a WHERE a.shortDesc LIKE :pattern", Advertisement.class);
    query.setParameter("pattern", "%" + pattern + "%");
    return query.getResultList();
  }
  
  public List<Basket> getAllBasket(String pattern)
  {
    Query query = this.entityManager.createQuery("SELECT a FROM Basket a WHERE a.shortDesc LIKE :pattern or a.name LIKE :pattern", Basket.class);
    query.setParameter("pattern", "%" + pattern + "%");
    return query.getResultList();
  }
  
  public List<Basket> getAdvFromBasket()
  {
    Query query = this.entityManager.createQuery("SELECT a FROM Basket a", Basket.class);
    return query.getResultList();
  }
  
  public void add(Advertisement adv)
  {
    try
    {
      this.entityManager.getTransaction().begin();
      this.entityManager.persist(adv);
      this.entityManager.getTransaction().commit();
    }
    catch (Exception ex)
    {
      this.entityManager.getTransaction().rollback();
      ex.printStackTrace();
    }
  }
  
  public void delete(long id, HttpServletRequest request)
  {
    try
    {
      this.entityManager.getTransaction().begin();
      Advertisement adv = (Advertisement)this.entityManager.find(Advertisement.class, Long.valueOf(id));
      Basket basket = new Basket(adv.getName(), adv.getShortDesc(), adv.getLongDesc(), adv.getPhone(), adv.getPrice(), adv.getPhoto());
      User user = (User)request.getSession().getAttribute("user");
      basket.setUser(user);
      this.entityManager.remove(adv);
      this.entityManager.persist(basket);
      this.entityManager.getTransaction().commit();
    }
    catch (Exception ex)
    {
      this.entityManager.getTransaction().rollback();
      ex.printStackTrace();
    }
  }
  
  public void deleteByCheckbox(List<Long> idList)
  {
    try
    {
      this.entityManager.getTransaction().begin();
      for (Long id : idList)
      {
        Advertisement adv = (Advertisement)this.entityManager.find(Advertisement.class, id);
        Basket basket = new Basket(adv.getName(), adv.getShortDesc(), adv.getLongDesc(), adv.getPhone(), adv.getPrice(), adv.getPhoto());
        this.entityManager.remove(adv);
        this.entityManager.persist(basket);
      }
      this.entityManager.getTransaction().commit();
    }
    catch (Exception ex)
    {
      this.entityManager.getTransaction().rollback();
      ex.printStackTrace();
    }
  }
  
  public void restore(long id, HttpServletRequest request)
  {
    try
    {
      this.entityManager.getTransaction().begin();
      Basket basket = (Basket)this.entityManager.find(Basket.class, Long.valueOf(id));
      Advertisement adv = new Advertisement(basket.getName(), basket.getShortDesc(), basket.getLongDesc(), basket.getPhone(), basket.getPrice(), basket.getPhoto());
      User user = (User)request.getSession().getAttribute("user");
      adv.setUser(user);
      this.entityManager.remove(basket);
      this.entityManager.persist(adv);
      this.entityManager.getTransaction().commit();
    }
    catch (Exception ex)
    {
      this.entityManager.getTransaction().rollback();
      ex.printStackTrace();
    }
  }
  
  public void restoreByCheckbox(List<Long> idList)
  {
    try
    {
      this.entityManager.getTransaction().begin();
      for (Long id : idList)
      {
        Basket basket = (Basket)this.entityManager.find(Basket.class, id);
        Advertisement adv = new Advertisement(basket.getName(), basket.getShortDesc(), basket.getLongDesc(), basket.getPhone(), basket.getPrice(), basket.getPhoto());
        this.entityManager.remove(basket);
        this.entityManager.persist(adv);
      }
      this.entityManager.getTransaction().commit();
    }
    catch (Exception ex)
    {
      this.entityManager.getTransaction().rollback();
      ex.printStackTrace();
    }
  }
  
  public byte[] getPhoto(long id)
  {
    try
    {
      Photo photo = (Photo)this.entityManager.find(Photo.class, Long.valueOf(id));
      return photo.getBody();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    return null;
  }
  
  public void truncate(long id)
  {
    try
    {
      this.entityManager.getTransaction().begin();
      Basket basket = (Basket)this.entityManager.find(Basket.class, Long.valueOf(id));
      this.entityManager.remove(basket);
      this.entityManager.getTransaction().commit();
    }
    catch (Exception ex)
    {
      this.entityManager.getTransaction().rollback();
      ex.printStackTrace();
    }
  }
}
