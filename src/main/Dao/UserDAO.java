package main.Dao;

import java.util.List;
import main.Model.User;

public abstract interface UserDAO
{
  public abstract void add(User paramUser);
  
  public abstract void delete(long paramLong);
  
  public abstract User getUserById(Integer paramInteger);
  
  public abstract List<User> getAllUsers();
  
  public abstract User getUserByLogin(String paramString);
}
