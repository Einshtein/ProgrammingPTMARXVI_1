package main.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Advs")
public class Advertisement
{
  @Id
  @GeneratedValue
  private long id;
  @Column(name="name")
  private String name;
  @Column(name="short_desc")
  private String shortDesc;
  @Column(name="long_desc")
  private String longDesc;
  @Column(name="phone")
  private String phone;
  @Column(name="price")
  private double price;
  @OneToOne(cascade={javax.persistence.CascadeType.ALL})
  @JoinColumn(name="photo_id")
  private Photo photo;
  @ManyToOne(cascade={javax.persistence.CascadeType.ALL})
  @JoinColumn(name="user_id")
  private User user;
  
  public Advertisement() {}
  
  public Advertisement(String name, String shortDesc, String longDesc, String phone, double price, Photo photo)
  {
    this.name = name;
    this.shortDesc = shortDesc;
    this.longDesc = longDesc;
    this.phone = phone;
    this.price = price;
    this.photo = photo;
  }
  
  public User getUser()
  {
    return this.user;
  }
  
  public void setUser(User user)
  {
    this.user = user;
  }
  
  public long getId()
  {
    return this.id;
  }
  
  public void setId(long id)
  {
    this.id = id;
  }
  
  public String getShortDesc()
  {
    return this.shortDesc;
  }
  
  public void setShortDesc(String shortDesc)
  {
    this.shortDesc = shortDesc;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public void setName(String name)
  {
    this.name = name;
  }
  
  public String getLongDesc()
  {
    return this.longDesc;
  }
  
  public void setLongDesc(String longDesc)
  {
    this.longDesc = longDesc;
  }
  
  public String getPhone()
  {
    return this.phone;
  }
  
  public void setPhone(String phone)
  {
    this.phone = phone;
  }
  
  public double getPrice()
  {
    return this.price;
  }
  
  public void setPrice(double price)
  {
    this.price = price;
  }
  
  public Photo getPhoto()
  {
    return this.photo;
  }
  
  public void setPhoto(Photo photo)
  {
    this.photo = photo;
  }
}
