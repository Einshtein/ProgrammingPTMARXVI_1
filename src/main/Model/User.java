package main.Model;

import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="User")
public class User
{
  @Id
  @GeneratedValue
  private long id;
  @Column(name="login")
  private String login;
  @Column(name="password")
  private String password;
  @Column(name="name")
  private String name;
  @Column(name="surname")
  private String surname;
  @OneToMany(mappedBy="user")
  private Collection<Advertisement> advertisement;
  
  public User(String login, String name, String password, String surname)
  {
    this.login = login;
    this.name = name;
    this.password = password;
    this.surname = surname;
  }
  
  public User() {}
  
  public long getId()
  {
    return this.id;
  }
  
  public void setId(long id)
  {
    this.id = id;
  }
  
  public String getLogin()
  {
    return this.login;
  }
  
  public void setLogin(String login)
  {
    this.login = login;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public void setName(String name)
  {
    this.name = name;
  }
  
  public String getPassword()
  {
    return this.password;
  }
  
  public void setPassword(String password)
  {
    this.password = password;
  }
  
  public String getSurname()
  {
    return this.surname;
  }
  
  public void setSurname(String surname)
  {
    this.surname = surname;
  }
  
  public Collection<Advertisement> getAdvertisement()
  {
    return this.advertisement;
  }
  
  public void setAdvertisement(Collection<Advertisement> advertisement)
  {
    this.advertisement = advertisement;
  }
}
